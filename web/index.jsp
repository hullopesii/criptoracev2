<%@page import="br.dawii.model.Contest"%>
<%@page import="java.util.List"%>
<%@page import="br.dawii.dao.ContestDAO"%>
<%
    
    ContestDAO contestDao = new ContestDAO();
    List<Contest> listContest = contestDao.listaTodos();
        
    String msgSucesso = (request.getAttribute("msgSucesso")!=null) ? request.getAttribute("msgSucesso").toString() : null;
    String msgError = (request.getAttribute("msgError")!=null) ? request.getAttribute("msgError").toString() : null;
    String nameUserCadastrado = (request.getAttribute("user")!=null) ? request.getAttribute("user").toString() : "";
%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="pt-BR">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="Content-Language" content="pt-br">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>CripoRace v2 - <%= listContest.get(0).getNomeContest()%> - <%= listContest.get(0).getLoc()%> </title>
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="js/base.js"></script>
        <!-- jQuery library -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

        <!-- Latest compiled JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="style/style.css">
    </head>
    <body>
        <div class="container">
            <img class="img-responsive logo center-block" src="img/logoG2.png" alt="CriptoRace">
        </div>
        <div class="container formulario">
            <%
                if(msgSucesso != null){
                    out.println("<div class=\"alert alert-success\" role=\"alert\">"+msgSucesso+"</div>");
                }
                if(msgError != null){
                    out.println("<div class=\"alert alert-danger\" role=\"alert\">"+msgError+"</div>");
                }
            %>    
            
            <form class="form-horizontal" action="./candidatoS" method="POST">
                <input type="hidden" name="cmd" value="login">
                <input type="hidden" name="idContest" value="<%= listContest.get(0).getIdContest()%>">
                <div class="form-group">                    
                    <input required type="text" class="form-control input-lg" id="usuario" placeholder="Nome de Usuário" value="<%= nameUserCadastrado %>" name="usuario">
                </div>
                <div class="form-group">              
                        <input required type="password" class="form-control input-lg" id="senha" placeholder="Entre com a senha" name="senha">
                </div>
                <div class="form-group">        
                        <button type="submit" class="btn btn-primary btn-lg btn-block">Entrar</button>
                </div>                
            </form>

            <form class="form-horizontal" action="./cadastrarCandidato.jsp" method="POST">
                <div class="form-group">  
                        <input type="hidden" name="cmd" value="cadastrar">
                        <button type="submit" class="btn btn-warning btn-lg btn-block">Cadastrar</button>
                </div>
            </form>
                <div class="form-group">        
                        <button type="submit" id="estouComMedo" class="btn btn-danger btn-lg btn-block" >Estou com medo!</button>
                </div>

        </div> <!-- // container -->
    </body>
    <script>
        $(".alert").click(function(){
            $(".alert").slideUp(500);
        });
        $("#estouComMedo").click(function(){
            $("#estouComMedo").html(estouComMedo());
        });
    </script>
</html>

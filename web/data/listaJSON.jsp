<%-- 
    Document   : listaJSON
    Created on : 08/11/2017, 17:22:48
    Author     : tatuapu
--%>

<%@page import="java.util.List"%>
<%@page import="br.dawii.model.Mensagem"%>
<%@page import="java.io.StringWriter"%>
<%@page import="org.json.simple.parser.JSONParser"%>
<%@page import="java.util.ArrayList"%>
<%@page import="org.json.simple.*"%>
<% 
JSONObject dados = new JSONObject();
JSONParser parser = new JSONParser();
String saida = "";
String classe = (request.getAttribute("classe")!=null) ? request.getAttribute("classe").toString() : "";
ArrayList<String> lista;
//out.println(classe);
if(classe.equalsIgnoreCase("msgsacanagem")){
    lista = (ArrayList<String>) request.getAttribute("lista-msgDaTelaDeLogin");
    int i = 1;
    JSONObject pai = new JSONObject();
    JsonArray filhos = new JsonArray();
    for(String st : lista){
        
        dados = new JSONObject();
        dados.put("msg",st.toString());
        
        filhos.add(dados);
        
    }
    pai.put("data", filhos);
    out.print(pai);
}else if(classe.equalsIgnoreCase("matriculaifg")){
    String resposta = request.getAttribute("respMatricula").toString();
    //out.println(resposta);
    if(resposta!=null){
        if(resposta.equalsIgnoreCase("ExisteMatricula"))
            out.print("false");
        else
            out.print("true");
    }
        
}else if(classe.equalsIgnoreCase("nomeusuario")){
    String resposta = request.getAttribute("respNome").toString();
    if(resposta!=null){
        if(resposta.equalsIgnoreCase("existe"))
            out.print("false");
        else
            out.print("true");
    }
}else if(classe.equalsIgnoreCase("nickusuario")){
    String resposta = request.getAttribute("respNick").toString();
    if(resposta!=null){
        if(resposta.equalsIgnoreCase("existe"))
            out.print("false");
        else
            out.print("true");
    }
}else if(classe.equalsIgnoreCase("passusuario")){
    String resposta = request.getAttribute("respPass").toString();
    if(resposta!=null){
        if(resposta.equalsIgnoreCase("fraca"))
            out.print("false");
        else
            out.print("true");
    }
}else if(classe.equals("tttttrrrreeegsdg")){
    List<Mensagem> listaM = (ArrayList<Mensagem>) request.getAttribute("listaMensagens");
    int i = 1;
    JSONObject pai = new JSONObject();
    JsonArray filhos = new JsonArray();
    for(Mensagem st : listaM){
        
        dados = new JSONObject();
        dados.put("msg",st.getMsg());
        
        filhos.add(dados);
        
    }
    pai.put("data", filhos);
    out.print(filhos);
}
%>

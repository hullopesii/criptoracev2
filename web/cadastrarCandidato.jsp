<%@page import="br.dawii.model.Contest"%>
<%@page import="java.util.List"%>
<%@page import="br.dawii.dao.ContestDAO"%>
<%
    HttpSession sessao = request.getSession(true);
    if (sessao.getAttribute("idUsuario") != null) {
//        response.sendRedirect("./inicial.jsp");
    }
    ContestDAO contestDao = new ContestDAO();
    List<Contest> listContest = contestDao.listaTodos();
    String mensagem = (String) request.getAttribute("mensagem");
    if (mensagem != null) {
        out.println(mensagem);
    }
    String msgError = (request.getAttribute("msgError")!=null) ? request.getAttribute("msgError").toString() : null;
%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="pt-BR">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="Content-Language" content="pt-br">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>CripoRace v2 - Cadastro de Novo Competidor -<%= listContest.get(0).getNomeContest()%> </title>
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="js/base.js"></script>
        <!-- jQuery library -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

        <!-- Latest compiled JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="style/style.css">
        <script src="jquery/jquery.validate.min.js"></script>
    </head>
    <body>
        <div class="container">
            <img class="img-responsive logo center-block" src="img/logoG2.png" alt="CriptoRace">
        </div>
        <div class="container formulario">
            <%
                if(msgError != null){
                    out.println("<div id='mensageiro' class=\"alert alert-danger\" role=\"alert\">"
                            + "<span class=\"glyphicon glyphicon-exclamation-sign\" aria-hidden=\"true\"></span>"
                            +msgError+"</div>");
                }
            %>   
            <form class="form-horizontal" action="./candidatoS" id="formCadastro" method="POST">
                <input type="hidden" name="cmd" value="cadastraCandidato">
                <input type="hidden" name="idContest" value="<%= listContest.get(0).getIdContest()%>">
                <div class="form-group">                    
                        <input type="text" class="form-control input-lg" id="nomeCompleto" placeholder="Nome Completo" name="nomeCompleto">
                </div>
                <div class="form-group">                    
                        <input type="text" class="form-control input-lg" id="nick" placeholder="Nick" name="nick">
                </div>
                <div class="form-group">                    
                        <input type="text" class="form-control input-lg" id="matriculaIFG" placeholder="Matrícula IFG" name="matriculaIFG">
                </div>
                <div class="form-group">                    
                        <input type="text" class="form-control input-lg" id="user" placeholder="Nome de Usuário" name="user">
                </div>
                <div class="form-group">              
                        <input type="password" class="form-control input-lg" id="passWd" placeholder="Entre com a senha" name="passWd">
                </div>
                
                <div class="form-group">                    
                        <input type="text" class="form-control input-lg error" id="user2" placeholder="Repita o Nome de Usuário" name="user2">
                </div>
                <div class="form-group">              
                        <input type="password" class="form-control input-lg error" id="passWd2" placeholder="Repita a senha" name="passWd2">
                </div>
                
                <div class="form-group">        
                        <button type="submit" id="btnSubmit" class="btn btn-primary btn-lg btn-block">Cadastrar</button>
                </div>  
                
                
            </div>
                
            </form>


        </div> <!-- // container -->
    </body>
    <script>
        
        $(".alert").click(function(){
            $(".alert").slideUp(500);
        });
    </script>
    <script>
            //submetendo um formulario com jquery!!
        var idContest = <%= listContest.get(0).getIdContest()%>;
        $().ready(function() { 
            // validate the comment form when it is submitted
            $("#formCadastro").validate({
                rules: {
                    nomeCompleto: {
                        required: true,
                        minlength: 10
                    },
                    nick: {
                        required: true,
                        minlength: 5,
                        remote: "./checkData?cmd=checkNick&idContest="+idContest
                    },
                    matriculaIFG: {
                        required: true,
                        minlength: 13,
                        number: true,
                        remote: "./checkData?cmd=checkMatriculaIFG&idContest="+idContest
                    },
                    user: {
                        required: true,
                        minlength: 4,
                        remote: "./checkData?cmd=checkNome&idContest="+idContest
                    },
                    passWd: {
                        required: true,
                        minlength: 6,
                        remote: "checkData?cmd=checkPassWd"
                    },
                    user2:{
                        required: true,
                        equalTo: "#user"
                    },
                    passWd2:{
                        required: true,
                        equalTo: "#passWd"
                    }
                },
                messages: {
                    nomeCompleto: {
                        required: "Por favor, informe seu nome",
                        minlength: "Informe um nome válido"
                    },
                    nick: {
                        required: "É necessário informar um nick válido",
                        minlength: "Informe um nick válido - pelo menos 5 caracteres",
                        remote: "Nick inválido ou já em uso nesta competição!"
                    },
                    matriculaIFG: {
                        required: "A mensagem não pode ficar em branco",
                        minlength: "Informe uma matrícula válida",
                        remote: "Matrícula já cadastrada"
                    },
                    user: {
                        required: "Informe um Nome de Usuário para Logar",
                        minlength: "Pelo menos 4 caracteres",
                        remote: "Nome de usário inválido!"
                    },
                    passWd: {
                        required: "Informe Uma Senha!",
                        minlength : "Mínimo de 6 caracteres",
                        remote: "Senha insegura"
                    },
                    user2:{
                        required: "Repita o nome de usuário",
                        equalTo: "Nomes de usuário diferentes"
                    },
                    passWd2:{
                        required: "Repita a senha",
                        equalTo: "Senhas diferentes"
                    }
                },
                submitHandler: function() { 
                    $("#formCadastro").submit();
                }
            });
        });    
        </script>
        
</html>

<%@page import="java.util.ArrayList"%>
<%@page import="br.dawii.model.RankingGeral"%>
<%@page import="br.dawii.model.Contest"%>
<%@page import="java.util.List"%>
<%@page import="br.dawii.dao.ContestDAO"%>
<%
    HttpSession sessao = request.getSession(true);
    int idCandidato = (sessao.getAttribute("idUsuario") != null) ? Integer.parseInt(sessao.getAttribute("idUsuario").toString()) : 0;

    if (idCandidato == 0) {
        response.sendRedirect("/");
    }

    ContestDAO contestDao = new ContestDAO();
    List<Contest> listContest = contestDao.listaTodos();

    String msgSucesso = (request.getAttribute("msgSucesso") != null) ? request.getAttribute("msgSucesso").toString() : null;
    String msgError = (request.getAttribute("msgError") != null) ? request.getAttribute("msgError").toString() : null;
    String tipoRanking = (request.getAttribute("tipo-ranking") != null) ? request.getAttribute("tipo-ranking").toString() : "";
    
    ArrayList<RankingGeral> listaRankingGeral = new ArrayList<RankingGeral>();
    ArrayList<RankingGeral> listaRankingGeralZ = new ArrayList<RankingGeral>();
    
    if(tipoRanking !=""){
        listaRankingGeral = (ArrayList<RankingGeral>) request.getAttribute("lista-ranking-top");
        listaRankingGeralZ = (ArrayList<RankingGeral>) request.getAttribute("lista-ranking-zop");
    }
%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="pt-BR">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="Content-Language" content="pt-br">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>CripoRace v2 - <%= listContest.get(0).getNomeContest()%> - <%= listContest.get(0).getLoc()%> </title>
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        
        <script src="js/base.js"></script>
        <!-- jQuery library -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

        <!-- Latest compiled JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="style/style.css">
        <script src="jquery/enscroll-0.6.0.min.js" type="text/javascript"></script>
        <script>
            var idContest = <%=listContest.get(0).getIdContest() %>;
        </script>
    </head>
    <body>
        <div class="container">
            <a href="./home.jsp"><img class="img-responsive logo logoP center-block" src="img/logoG2.png" alt="CriptoRace"></a>    
        </div>
        <div class="container formulario">

            <form class="form-horizontal" action="./home.jsp" method="POST">
                <div class="form-group">  
                    <button type="submit" class="btn btn-lg btn-warning btn-block">Voltar</button>
                </div>
            </form>


        </div> <!-- // container -->
        <% if (listaRankingGeral!= null && listaRankingGeral.size()>0){ %>
        <section class="container formulario">
            <div class="row">
                
                 <div class=" col-sm-6 col-md-6">        
                <div class="panel panel-primary">
                    <!-- Default panel contents -->
                    <div class="panel-heading">Top 10</div>

                    <!-- Table -->
                    <table class="table">
                        <thead>
                          <th>#</th>
                          <th>nick</th>
                          <th>pontos</th>
                        </thead>
                        <tbody>
                          <% 
                              for(int i=1;i<listaRankingGeral.size()+1;i++){
                                  out.println("<tr>");
                                  out.println("   <td>"+ i +"º</td>");
                                  out.println("   <td>"+ listaRankingGeral.get(i-1).getNick() +"</td>");
                                  out.println("   <td>"+ listaRankingGeral.get(i-1).getTotalDePontos() +"º</td>");
                                  out.println("</tr>");
                              }
                          %>    
                        </tbody>
                    </table>
                </div>
                </div>
                        
                <div class=" col-sm-6 col-md-6">        
                <div class="panel panel-danger vermelho">
                    <!-- Default panel contents -->
                    <div class="panel-heading">Zop 10</div>

                    <!-- Table -->
                    <table class="table">
                        <thead>
                          <th>#</th>
                          <th>nick</th>
                          <th>erros</th>
                        </thead>
                        <tbody>
                          <% 
                              for(int i=1;i<listaRankingGeralZ.size()+1;i++){
                                  out.println("<tr>");
                                  out.println("   <td>"+ i +"º</td>");
                                  out.println("   <td>"+ listaRankingGeralZ.get(i-1).getNick() +"</td>");
                                  out.println("   <td>"+ listaRankingGeralZ.get(i-1).getTotalDePontos() +"</td>");
                                  out.println("</tr>");
                              }
                          %>    
                        </tbody>
                    </table>
                </div>
                </div>        
            </div>        
        </section>
        <% } %>
        
        <!-- chat -->
        <%@include file="template-footer-chat.jsp"%>
        <!-- //chat -->
        
    </body>
    
</html>

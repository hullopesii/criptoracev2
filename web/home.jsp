<%@page import="br.dawii.model.Contest"%>
<%@page import="java.util.List"%>
<%@page import="br.dawii.dao.ContestDAO"%>
<%
    HttpSession sessao = request.getSession(true);
    int idCandidato = (sessao.getAttribute("idUsuario") != null) ? Integer.parseInt(sessao.getAttribute("idUsuario").toString()) : 0;

    if (idCandidato == 0) {
        response.sendRedirect("/");
    }

    ContestDAO contestDao = new ContestDAO();
    List<Contest> listContest = contestDao.listaTodos();

    String msgSucesso = (request.getAttribute("msgSucesso") != null) ? request.getAttribute("msgSucesso").toString() : null;
    String msgError = (request.getAttribute("msgError") != null) ? request.getAttribute("msgError").toString() : null;
    String nameUserCadastrado = (request.getAttribute("user") != null) ? request.getAttribute("user").toString() : "";
%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="pt-BR">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="Content-Language" content="pt-br">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>CripoRace v2 - <%= listContest.get(0).getNomeContest()%> - <%= listContest.get(0).getLoc()%> </title>
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="js/base.js"></script>
        <!-- jQuery library -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

        <!-- Latest compiled JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="style/style.css">
        <script src="jquery/enscroll-0.6.0.min.js" type="text/javascript"></script>
        <script>
            var idContest = <%=listContest.get(0).getIdContest() %>;
        </script>
    </head>
    <body>
        <div class="container">
            <a href="./home.jsp"><img class="img-responsive logo center-block" src="img/logoG2.png" alt="CriptoRace"></a>    
        </div>
        <div class="container formulario">
            <%
                if (msgSucesso != null) {
                    out.println("<div class=\"alert alert-success\" role=\"alert\">" + msgSucesso + "</div>");
                }
                if (msgError != null) {
                    out.println("<div class=\"alert alert-danger\" role=\"alert\">" + msgError + "</div>");
                }
            %>    

            <form class="form-horizontal" action="./controlador.pvh" method="POST">
                <div class="form-group">
                    <input type="hidden" name="cmd" value="showSubmitForm">
                    <button type="submit" class="btn btn-info btn-lg btn-block">Enviar Resposta</button>
                </div>
            </form>

            <form class="form-horizontal" action="./controlador.pvh" method="POST">
                <div class="form-group">  
                    <input type="hidden" name="cmd" value="showRanking">
                    <button type="submit" class="btn btn-lg btn-primary btn-block">Ranking Geral</button>
                </div>
            </form>


        </div> <!-- // container -->
        <footer>
            <nav class="nav-chat" id="nav-chat">
                <div class="nav-chat-header-button" id="botao-abre-chat">
                    <span class="glyphicon glyphicon-bullhorn img-btn-chat" aria-hidden="true"></span>
                </div>
                    
                <div class="porta-mural-home">
                    <!--<iframe id="frame-chat" src="./mensageiroS?asfas5asafa2uyituuey8778h=uedwuakwujask98e93ikkwiksjhw" ></iframe>-->
                    <iframe id="frame-chat" src="./muralList.jsp" ></iframe>
                </div>
                <div class="container">
                    <form id="form-chat" class="form-horizontal">
                        <div class="form-group">  
                            <div class="chat-controller">
                                <div class="campo-mensagem">
                                    
                                    <input type="text" class="form-control input-xs" id="mensagem" placeholder="Digite sua Mensagem" name="mensagem">
                                </div>
                                <div class="campo-botao">        
                                    <button type="button" id="btn-envia-msg" class="btn btn-info btn-block" data-toggle="collapse" data-target="#painel-chat" aria-expanded="false">
                                          <span class="glyphicon glyphicon-bullhorn img-btn-chat" aria-hidden="true"></span>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>    
            </nav>
            
        </footer>
    </body>
    <script>
        var checaTelaChat = false;
        $("#nav-chat").css({"bottom":"-190px"});
        $(".alert").click(function () {
            $(".alert").slideUp(500);
        });
        $("#botao-abre-chat").click(function(){
            if(!checaTelaChat){
                $("#nav-chat").css({"bottom":"0px"});
                checaTelaChat = true;
            }else{
                $("#nav-chat").css({"bottom":"-190px"});
                checaTelaChat = false;
            }    
        });
        
        $("#btn-envia-msg").click(function(){
            msg = $("#mensagem").val();
            if(msg!=""){
                $.ajax({url: "./mensageiroS?asfas5asafa2uyituuey8778h=uieksyiwjncmueioisok&ucjdjwiskiwkwi="+msg, success: function(result){
                    $("#mensagem").val("");
                    $("#mensagem").attr("disabled", true);
                    $("#mensagem").attr("placeholder", "Enviando mensagem...");
                    setTimeout(function() { 
                        $("#mensagem").removeAttr("disabled"); 
                        $("#mensagem").attr("placeholder", "Digite sua Mensagem");
                    }, 6000);
                }});
            }
        });
        
//        setInterval(function(){ 
//            $("#frame-chat").attr('src', "./mensageiroS?asfas5asafa2uyituuey8778h=uedwuakwujask98e93ikkwiksjhw")
//        }, 6000);
    </script>
</html>

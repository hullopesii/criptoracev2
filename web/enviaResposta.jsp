<%@page import="br.dawii.model.Contest"%>
<%@page import="java.util.List"%>
<%@page import="br.dawii.dao.ContestDAO"%>
<%
    HttpSession sessao = request.getSession(true);
    int idCandidato = (sessao.getAttribute("idUsuario") != null) ? Integer.parseInt(sessao.getAttribute("idUsuario").toString()) : 0;

    if (idCandidato == 0) {
        response.sendRedirect("/");
    }

    ContestDAO contestDao = new ContestDAO();
    List<Contest> listContest = contestDao.listaTodos();

    String msgSucesso = (request.getAttribute("msgSucesso") != null) ? request.getAttribute("msgSucesso").toString() : null;
    String msgError = (request.getAttribute("msgError") != null) ? request.getAttribute("msgError").toString() : null;
    String nameUserCadastrado = (request.getAttribute("user") != null) ? request.getAttribute("user").toString() : "";
    int pontuacaoObtida = (request.getAttribute("submited-response") != null) ? Integer.parseInt(request.getAttribute("submited-response").toString()) : -100000;

%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="pt-BR">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="Content-Language" content="pt-br">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>CripoRace v2 - <%= listContest.get(0).getNomeContest()%> - <%= listContest.get(0).getLoc()%> </title>
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        
        <script src="js/base.js"></script>
        <!-- jQuery library -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

        <!-- Latest compiled JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="style/style.css">
        <script src="jquery/enscroll-0.6.0.min.js" type="text/javascript"></script>
        <script>
            var idContest = <%=listContest.get(0).getIdContest() %>;
        </script>
    </head>
    <body>
        <div class="container">
            <a href="./home.jsp"><img class="img-responsive logo center-block" src="img/logoG2.png" alt="CriptoRace"></a>    
        </div>
        <div class="container formulario">
            <%
                String portaFormClass = "";
                if (msgSucesso != null) {
                    out.println("<div class=\"alert alert-success\" role=\"alert\">" + msgSucesso + "</div>");
                }
                if (msgError != null) {
                    out.println("<div class=\"alert alert-danger\" role=\"alert\">" + msgError + "</div>");
                }
                if (pontuacaoObtida > 0) {
                    out.println("<div class=\"alert alert-success\" role=\"alert\"><h3>Parabéns! Você Acertou!<br>Ganhou " + pontuacaoObtida + " pontos !!!</h3></div>");
                    portaFormClass = "azul";
                }
                if (pontuacaoObtida == 0){
                    out.println("<div class=\"alert alert-danger\" role=\"alert\"><h3>Eita! Você Errou!<br>Não desanime!!!!<h3></div>");
                    portaFormClass = "rosa";
                }
                if (pontuacaoObtida == -1){
                    out.println("<div class='alert alert-danger' role='alert'><h3>Uha! Você Já enviou esta resposta e acertou!<br>Vá procurar outras!!!!</h3></div>");
                    portaFormClass = "amarelo";
                }
            %>    
            <div class="porta-form <%=portaFormClass%>">
            <form class="form-horizontal" action="./controlador.pvh" method="POST">
                <div class="form-group">                    
                    <input required type="text" class="form-control input-lg" id="resposta" placeholder="Digite uma resposta" name="resposta">
                </div>
                <div class="form-group">
                    <input type="hidden" name="cmd" value="submitResp">                    
                    <button type="submit" class="btn btn-info btn-lg btn-block">Enviar Resposta</button>
                </div>
            </form>
            
            <form class="form-horizontal" action="./controlador.pvh" method="POST">
                <div class="form-group">  
                    <input type="hidden" name="cmd" value="showRanking">
                    <button type="submit" class="btn btn-lg btn-primary btn-block">Ranking Geral</button>
                </div>
            </form>

            <form class="form-horizontal" action="./home.jsp" method="POST">
                <div class="form-group">  
                    <button type="submit" class="btn btn-lg btn-warning btn-block">Voltar</button>
                </div>
            </form>
            </div><!--// porta form-->


        </div> <!-- // container -->
        
        <!-- chat -->
        <%@include file="template-footer-chat.jsp"%>
        <!-- //chat -->
        
    </body>
    
</html>

<footer>
    <nav class="nav-chat" id="nav-chat">
        <div class="nav-chat-header-button" id="botao-abre-chat">
            <span class="glyphicon glyphicon-bullhorn img-btn-chat" aria-hidden="true"></span>
        </div>

        <div class="porta-mural-home">
            <!--<iframe id="frame-chat" src="./mensageiroS?asfas5asafa2uyituuey8778h=uedwuakwujask98e93ikkwiksjhw" ></iframe>-->
            <iframe id="frame-chat" src="./muralList.jsp" ></iframe>
        </div>
        <div class="container">
            <form id="form-chat" class="form-horizontal">
                <div class="form-group">  
                    <div class="chat-controller">
                        <div class="campo-mensagem">

                            <input type="text" class="form-control input-xs" id="mensagem" placeholder="Digite sua Mensagem" name="mensagem">
                        </div>
                        <div class="campo-botao">        
                            <button type="button" id="btn-envia-msg" class="btn btn-info btn-block" data-toggle="collapse" data-target="#painel-chat" aria-expanded="false">
                                <span class="glyphicon glyphicon-bullhorn img-btn-chat" aria-hidden="true"></span>
                            </button>
                        </div>
                    </div>
                </div>
            </form>
        </div>    
    </nav>

</footer>

<script>
    $('#form-chat').on('keyup keypress', function(e) {
        var keyCode = e.keyCode || e.which;
        if (keyCode === 13) { 
          e.preventDefault();
          return false;
        }
      });
      
    var checaTelaChat = false;
    $("#nav-chat").css({"bottom": "-190px"});
    $(".alert").click(function () {
        $(".alert").slideUp(500);
    });
    $("#botao-abre-chat").click(function () {
        if (!checaTelaChat) {
            $("#nav-chat").css({"bottom": "0px"});
            checaTelaChat = true;
        } else {
            $("#nav-chat").css({"bottom": "-190px"});
            checaTelaChat = false;
        }
    });

    $("#btn-envia-msg").click(function () {
        msg = $("#mensagem").val();
        if (msg != "") {
            $.ajax({url: "./mensageiroS?asfas5asafa2uyituuey8778h=uieksyiwjncmueioisok&ucjdjwiskiwkwi=" + msg, success: function (result) {
                    $("#mensagem").val("");
                    $("#mensagem").attr("disabled", true);
                    $("#mensagem").attr("placeholder", "Enviando mensagem...");
                    setTimeout(function () {
                        $("#mensagem").removeAttr("disabled");
                        $("#mensagem").attr("placeholder", "Digite sua Mensagem");
                    }, 6000);
                }});
        }
    });

</script>
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.tatuapu.util;

import br.dawii.model.Candidato;
import br.dawii.model.DataBuilder;
import br.dawii.model.StrategyModel;
import java.util.Map;
import java.util.Map.Entry;
import javax.servlet.http.HttpServletRequest;
import br.dawii.model.DataBuilderModel;

/**
 *
 * @author tatuapu
 */
public class TrataForm {

    public static Object getObjectFrom(HttpServletRequest request, DataBuilderModel dbm) {
        Object retorno;
        retorno = dbm.autoConstructor(request.getParameterMap());
        return retorno;
    }
    
}

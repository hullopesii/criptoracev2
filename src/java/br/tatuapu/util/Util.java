package br.tatuapu.util;

import br.dawii.model.Candidato;
import java.lang.reflect.Field;

/**
 *
 * @author tatuapu
 */
public class Util {
    
    public static String[] getListaDeColunas(Class classe) {
        //String[] colunas = {"idCandidato","nomeCompleto","nick","matriculaIFG","cpf","user","passWd","idContest"};
        Field[] fields = classe.getFields();
        String[] colunas = new String[fields.length];
        int i=0;
        for(Field f:fields){
            colunas[i]=f.getName();
            i++;
        }
        return colunas;
    } 
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.dawii.controller;

import br.dawii.dao.CandidatoDAO;
import br.dawii.model.Candidato;
import br.dawii.model.StrategyModel;
import br.tatuapu.util.TrataForm;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author tatuapu
 */
@WebServlet(name = "CandidatoServlet", urlPatterns = {"/candidatoS"})
public class CandidatoServlet extends ControllerServlet {
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        this.request = request;
        this.response = response;
        
        CandidatoDAO candidatoDAO = new CandidatoDAO();
         RequestDispatcher rd = null;
         String cmd = (request.getParameter("cmd")!=null) ? request.getParameter("cmd") : "";
         
         if(cmd.equalsIgnoreCase("listar")){
            try{
                List<Candidato> listaCandidatos = candidatoDAO.listaTodos(); 
                setViewData((ArrayList) listaCandidatos, "listaCandidatos", "Candidato");
                abreViewDestino("/candidatos.jsp");
            }catch(Exception e){
                    
            }    
         }else if(cmd.equalsIgnoreCase("login")){
             String usuario = request.getParameter("usuario");
             String pass = request.getParameter("senha");
             int idContest = Integer.parseInt(request.getParameter("idContest"));
             Candidato cd = new Candidato(0,null,null,
                     null,usuario, pass, idContest);
            try {
                int r = podeFazerLogin(cd);
                if(r>0){
                    HttpSession sessao = request.getSession(true);
                    sessao.setAttribute("idUsuario",r);
                    sessao.setAttribute("idContest",idContest);
                    redirecionaParaViewDestino("./home.jsp");
                }else{
                    setViewData("Usuário Inexistente ou Dados Incorretos!","msgError",cmd);
                    abreViewDestino("/");
                }
            } catch (Exception ex) {
                //throw new ServletException("Erro login"+ex.getMessage());
                setViewData("Erro ao processar Login! <br><b>Tente novamente!</b>","msgError",cmd);
                abreViewDestino("/");
            }
             
         }else if(cmd.equalsIgnoreCase("cadastraCandidato")){
             Candidato candidatoNovo = (Candidato) TrataForm.getObjectFrom(request, new Candidato());
             if(candidatoNovo !=null){
                 try{
                    candidatoDAO.salvar(candidatoNovo);
                    setViewData("Cadastro realizado com sucesso!","msgSucesso",cmd);
                    setViewData(candidatoNovo.getUser(),"user","Candidato");
                    abreViewDestino("./");
                 }catch(Exception e){
                     setViewData("Erro ao processar cadastro o candidato! <br>"+e.getMessage(),
                             "msgError",cmd);
                     abreViewDestino("./cadastrarCandidato.jsp");
                 }   
             }
         }else{
             abreViewDestino("");
         }    
        
        //abreViewDestino("/");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(CandidatoServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(CandidatoServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    private int podeFazerLogin(Candidato cd) throws Exception {        
            
             List<Candidato> listaCandidatos;
             CandidatoDAO dao = new CandidatoDAO();
            try {
                listaCandidatos = dao.procura((Object) cd);
                //List l = dao.procura((Object) cd);
                if(listaCandidatos.size()==1){
                    return listaCandidatos.get(0).getIdCandidato();
                }
            } catch (Exception ex) {
                throw new Exception("Erro ao tentar obter dados de login");
            } 
            return 0;
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.dawii.controller;

import br.dawii.dao.RankingDAO;
import br.dawii.dao.SubmeterRespostaDAO;
import br.dawii.model.RankingGeral;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author tatuapu
 */
@WebServlet(name = "ControladorGeral", urlPatterns = {"/controlador.pvh"})
public class ControladorGeral extends ControllerServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        this.request = request;
        this.response = response;
        
        HttpSession sessao = request.getSession(true);
        int idCandidato = (sessao.getAttribute("idUsuario") != null) ? Integer.parseInt(sessao.getAttribute("idUsuario").toString()) : 0;

        String viewDestino="-";
        String viewDestinoR="-"; //redireciona
        
        if (idCandidato == 0) {
            viewDestinoR = "/";
        }
        
        String resposta = (request.getParameter("resposta") != null) ? request.getParameter("resposta") : "";

        
        String cmd = (request.getParameter("cmd")!=null) ? request.getParameter("cmd").toLowerCase() : "";
        
                
        switch (cmd){
            case "showsubmitform":
                showSubmitForm();
                break;
            case "submitresp":
                setViewData(Integer.toString(validaResposta(resposta,idCandidato)), "submited-response", "submitresp");
                abreViewDestino("/enviaResposta.jsp");
                break;
            case "showranking":
                try{
                    setViewData("geral","tipo-ranking","ranking");
                    setViewData(getRankingTop(),"lista-ranking-top","ranking");
                    setViewData(getRankingZop(),"lista-ranking-zop","ranking");
                    //abreViewDestino("./ranking.jsp");
                    viewDestino = "./ranking.jsp";
                    viewDestinoR = "-";
                }catch(Exception e){
                    setViewData("Erro ao tentar obter dados do ranking.<br> "+e.getMessage(), "msgError", "");
                    //abreViewDestino("./home.jsp");
                    viewDestino = "./home.jsp";
                    viewDestinoR="-";
                }    
                break;
            default:
                viewDestinoR = "/";
                viewDestino=null;
                break;
        }
        if(viewDestinoR!="-")
            redirecionaParaViewDestino(viewDestinoR);
        else if(viewDestino!="-")
            abreViewDestino(viewDestino);
        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    private void showSubmitForm() throws ServletException, IOException {
        try {
            redirecionaParaViewDestino("enviaResposta.jsp");
        } catch (IOException ex) {
            setViewData("Erro ao acessar formulário de envio de resposta","msgError","");
            abreViewDestino("./home.jsp");
        }
    }

    private int validaResposta(String resp,int idCandidato) {
        SubmeterRespostaDAO dao = new SubmeterRespostaDAO();
        int pontuacao =0;
        try {
            pontuacao = dao.validaResposta(resp, idCandidato);
        } catch (Exception ex) {
            Logger.getLogger(ControladorGeral.class.getName()).log(Level.SEVERE, null, ex);
        }
        return pontuacao;
    }

    private ArrayList getRankingTop() throws Exception {
        RankingDAO dao = new RankingDAO();
        ArrayList<RankingGeral> listaPositiva = (ArrayList<RankingGeral>) dao.buscaRankingGeralPositivo();
        return listaPositiva;
    }

    private ArrayList getRankingZop() throws Exception {
        RankingDAO dao = new RankingDAO();
        ArrayList<RankingGeral> listaNegativa = (ArrayList<RankingGeral>) dao.buscaRankingGeralNegativo();
        return listaNegativa;
    }

}

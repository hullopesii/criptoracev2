/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.dawii.controller;

import br.dawii.dao.CandidatoDAO;
import br.dawii.model.Candidato;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author tatuapu
 */
@WebServlet(name = "CheckDataServlet", urlPatterns = {"/checkData"})
public class CheckDataServlet extends ControllerServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        this.request = request;
        this.response = response;
        
        String cmd = (request.getParameter("cmd")!=null) ? request.getParameter("cmd").toLowerCase().toString() : "";
        ArrayList listaRetorno = new ArrayList();
        //RequestDispatcher rd =  request.getRequestDispatcher("/home.jsp");
        int idContest;
        Candidato candidato;
        
        switch(cmd){
            case "checkmatriculaifg":
                String matricula = (request.getParameter("matriculaIFG")!=null) ? request.getParameter("matriculaIFG").toLowerCase().toString() : "";
                idContest = (request.getParameter("idContest")!=null) ? Integer.parseInt(request.getParameter("idContest").toString()) : 0;
                setViewData(checaExistenciaDe(matricula,idContest),"respMatricula","matriculaIFG");
                abreViewDestino("./data/listaJSON.jsp");
                break;
            case "checknome":
                String nome = (request.getParameter("user")!=null) ? request.getParameter("user").toLowerCase() : "";
                idContest = (request.getParameter("idContest")!=null) ? Integer.parseInt(request.getParameter("idContest")) : 0;
                candidato = new Candidato(0,null,null,null,nome, null, idContest);
                setViewData(checaExistenciaDe(candidato),"respNome","nomeUsuario");
                abreViewDestino("./data/listaJSON.jsp");
                break;
            case "checknick":
                String nick = (request.getParameter("nick")!=null) ? request.getParameter("nick").toLowerCase() : "";
                idContest = (request.getParameter("idContest")!=null) ? Integer.parseInt(request.getParameter("idContest")) : 0;
                candidato = new Candidato(0,null,nick,null,null, null, idContest);
                setViewData(checaExistenciaDe(candidato),"respNick","nickUsuario");
                abreViewDestino("./data/listaJSON.jsp");
                break;  
            case "checkpasswd":    
                String senha = (request.getParameter("passWd")!=null) ? request.getParameter("passWd").toLowerCase() : "";
                setViewData(checaForcaDa(senha),"respPass","passUsuario");
                abreViewDestino("./data/listaJSON.jsp");
            default:
                abreViewDestino("/");
        }
    }

    private String checaExistenciaDe(String matricula, int idContest) {
        CandidatoDAO dao = new CandidatoDAO();
        String resposta = null;
        
        try {
            if(dao.checaSeMatriculaExiste(matricula,idContest))
                resposta =  "ExisteMatricula";
            else
                resposta = "naoExisteMatricula";                
        } catch (Exception ex) {
            Logger.getLogger(CheckDataServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
        return resposta;    
    }

    private String checaExistenciaDe(Candidato candidato) {
        CandidatoDAO dao = new CandidatoDAO();
        String resposta = null;
        ArrayList<Candidato> lista = new ArrayList();
        try{

            lista = dao.procura(candidato);
        }catch(Exception e){
            System.out.println(e.getMessage());
            e = e;
        }
        if(lista.size()>0)
            resposta = "Existe";
        else
            resposta = "NaoExiste";
        return resposta;
    }

    private String checaForcaDa(String senha) {
        if(senha.matches("(?<!\\d)\\d{6,12}(?!\\d)")){
            return "Fraca";
        }
        else
            return "Forte";
    }

    

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.dawii.controller;

import br.dawii.dao.MensagensDAO;
import br.dawii.model.Mensagem;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author tatuapu
 */
@WebServlet(name = "MensageiroServlet", urlPatterns = {"/mensageiroS"})
public class MensageiroServlet extends ControllerServlet {
    
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        this.request = request;
        this.response = response;
        
        //nome de parametro zuadão para enganar invasor!
        String cmd = (request.getParameter("asfas5asafa2uyituuey8778h")!=null) ? request.getParameter("asfas5asafa2uyituuey8778h") : "";
        
        HttpSession sessao = request.getSession(true);
        int idCandidato = Integer.parseInt(sessao.getAttribute("idUsuario").toString());
        
        int idContest = Integer.parseInt(sessao.getAttribute("idContest").toString());
        
        if(cmd.equals("uedwuakwujask98e93ikkwiksjhw")){
            MensagensDAO dao = new MensagensDAO(idContest);
            try {
                List<Mensagem> mensagens = dao.listaTodos();
                setViewData((ArrayList) mensagens,"listaMensagens",cmd);
                abreViewDestino("/mostraMural.jsp");
            } catch (Exception ex) {
                setViewData("erro","msgError",cmd);
                abreViewDestino("/mostraMural.jsp");
            }
        }else if(cmd.equals("uieksyiwjncmueioisok")){
            String msg = (request.getParameter("ucjdjwiskiwkwi")!=null) ? request.getParameter("ucjdjwiskiwkwi") : null;
            if(msg!=null){
                MensagensDAO dao = new MensagensDAO(idContest);
                try {
                    dao.salvar(new Mensagem(msg,idContest,idCandidato));
                } catch (Exception ex) {
                    Logger.getLogger(MensageiroServlet.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }else if(cmd.equals("tttttrrrreeegsdg")){
            //gerando lista para json
            MensagensDAO dao = new MensagensDAO(idContest);
            try {
                List<Mensagem> mensagens = dao.listaTodos();
                setViewData((ArrayList) mensagens,"listaMensagens",cmd);
                abreViewDestino("./data/listaJSON.jsp");
            } catch (Exception ex) {
                setViewData("erro","msgError",cmd);
                abreViewDestino("/mostraMural.jsp");
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

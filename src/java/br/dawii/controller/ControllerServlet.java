/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.dawii.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author tatuapu
 */
public abstract class ControllerServlet extends HttpServlet {
   
    HttpServletRequest request;
    HttpServletResponse response;

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    protected abstract void processRequest(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException;

    protected void setViewData(ArrayList lista, String nmParametro, String classe) {
        this.request.setAttribute(nmParametro, lista);
        this.request.setAttribute("classe", classe);
    }
    protected void setViewData(String valor, String nmParametro, String classe) {
        this.request.setAttribute(nmParametro, valor);
        this.request.setAttribute("classe", classe);
    }

    protected void abreViewDestino(String urlDestino) throws ServletException, IOException {
        if(urlDestino != "")
            this.request.getRequestDispatcher(urlDestino).forward(this.request, this.response);
        else
            this.response.sendRedirect("./");
    }
    protected void redirecionaParaViewDestino(String urlDestino) throws IOException{
        this.response.sendRedirect(urlDestino);
    }
}

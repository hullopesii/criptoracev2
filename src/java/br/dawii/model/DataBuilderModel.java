/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.dawii.model;

import java.util.Map;

/**
 *
 * @author tatuapu
 */
public interface DataBuilderModel {

    public String[] getListaDeColunas();
    public Object autoConstructor(Map<String, String[]> parameterMap);
    
}

package br.dawii.model;

/**
 *
 * @author tatuapu
 */
public class Contest {
    private final int idContest;
    private final String nomeContest;
    private final String status;
    private final String loc;
    public Contest(int idC, String nC, String status, String loc){
        this.idContest = idC;
        this.nomeContest = nC;
        this.status = status;
        this.loc = loc;
    }

    /**
     * @return the idContest
     */
    public int getIdContest() {
        return idContest;
    }

    /**
     * @return the nomeContest
     */
    public String getNomeContest() {
        return nomeContest;
    }

    /**
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @return the loc
     */
    public String getLoc() {
        return loc;
    }
}

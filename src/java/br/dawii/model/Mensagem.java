/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.dawii.model;

/**
 *
 * @author rafael
 */
public class Mensagem {
    String msg;
    int idC, idMsg, idCandidato;
    String nomeCandidato;

    public Mensagem(String msg, int idC) {
        this.msg = msg;
        this.idC = idC;
    }

    public Mensagem(String msg, int idC, int idCandidato){
        this.msg = msg;
        this.idC = idC;
        this.idCandidato = idCandidato;
    }
    public Mensagem(String msg, String nomeCandidato, int idC) {
        this.msg = msg;
        this.idC = idC;
        this.nomeCandidato = nomeCandidato;
    }
    
    public Mensagem(String msg, String nomeCandidato, int idC, int idMsg) {
        this.msg = msg;
        this.idC = idC;
        this.nomeCandidato = nomeCandidato;
        this.idMsg = idMsg;
    }
    

    public String getMsg() {
        return msg;
    }

    public int getIdC() {
        return idC;
    }
    
    public int getIdCandidato(){
        return this.idCandidato;
    }

    public String getNomeCandidato() {
        return nomeCandidato;
    }

    public int getIdMsg() {
        return idMsg;
    }
    
}

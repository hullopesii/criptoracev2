/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.dawii.model;

/**
 *
 * @author rafael
 */
public class RankingIndividual {
    private int totalPontos, erros, acertos;

    public RankingIndividual(int totalPontos, int erros, int acertos) {
    
        this.totalPontos = totalPontos;
        this.erros = erros;
        this.acertos = acertos;
    }

    public int getTotalPontos() {
        return totalPontos;
    }

    public int getErros() {
        return erros;
    }

    public int getAcertos() {
        return acertos;
    }
    
    
}

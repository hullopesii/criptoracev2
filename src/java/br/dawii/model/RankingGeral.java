/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.dawii.model;

/**
 *
 * @author rafael
 */
public class RankingGeral {
    private final int id;
    private final int totalDePontos;
    private final String nick;

    public RankingGeral(int id, String nick, int totalDePontos) {
        this.id = id;
        this.totalDePontos = totalDePontos;
        this.nick = nick;
    }

    public int getId() {
        return id;
    }

    public int getTotalDePontos() {
        return totalDePontos;
    }

    public String getNick() {
        return nick;
    }
    
}

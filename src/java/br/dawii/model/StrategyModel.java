package br.dawii.model;

import java.util.Map;

/**
 * Interface para auto construção de objetos
 * permitindo uso de pattern Stragety (GoF)
 * @author tatuapu
 */
public interface StrategyModel {
    StrategyModel autoConstructor(Map<String, String[]> entryMap);
}

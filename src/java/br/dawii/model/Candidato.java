/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.dawii.model;

import br.tatuapu.util.Util;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author fred
 */
public class Candidato implements DataBuilderModel {
    private int       idCandidato;
    private String    nomeCompleto;
    private String    nick;
    private String    matriculaIFG;
    private String    user;
    private String    passWd;
    private int       idContest;
    
    public Candidato(){
        
    }
    
    public Candidato(int idCandidato, String nomeCompleto, 
            String nick, String matriculaIFG, 
            String user, String passWd, int idContest) {
        this.idCandidato = idCandidato;
        this.nomeCompleto = nomeCompleto;
        this.nick = nick;
        this.matriculaIFG = matriculaIFG;
        this.user = user;
        this.passWd = passWd;
        this.idContest = idContest;
    }

    public Candidato(int aInt, String string, String string0, String string1, String string2, String string3, String string4, int aInt0) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public int getIdCandidato() {
        return idCandidato;
    }

    public String getNomeCompleto() {
        return nomeCompleto;
    }

    public String getNick() {
        return nick;
    }

    public String getMatriculaIFG() {
        return matriculaIFG;
    }

    public String getUser() {
        return user;
    }

    public String getPassWd() {
        return passWd;
    }
    public int getIdContest(){
        return idContest;
    } 
    
    @Override    
    public Candidato autoConstructor(Map<String, String[]> entryMap) {
        Map<String, String> valores = new HashMap<String, String>();
        Candidato cand;
        try{
            for (Map.Entry<String, String[]> entry : entryMap.entrySet()) {
                String chave = entry.getKey();
                String valor = entry.getValue()[0];
                if(contemChave(chave)){
                    valores.put(chave, valor);
                }            
            }
            String[] listaDeColunas = this.getListaDeColunas();

            cand = new Candidato(0,valores.get(listaDeColunas[1]),
            valores.get(listaDeColunas[2]),valores.get(listaDeColunas[3]),
            valores.get(listaDeColunas[4]),valores.get(listaDeColunas[5]),
            Integer.parseInt(valores.get(listaDeColunas[6])));
        }catch(Exception e){
            return null;
        }    
        
        return cand;
    }
    @Override
    public String[] getListaDeColunas() {
        String[] colunas = {"idCandidato","nomeCompleto","nick","matriculaIFG","user","passWd","idContest"};
        return colunas;
    }

    private boolean contemChave(String key) {
        String[] listaDeColunas = this.getListaDeColunas();
        
        for(int i=0;i<listaDeColunas.length;i++){
            if(listaDeColunas[i].equals(key))
                return true;
        }
        return false;
    }
}

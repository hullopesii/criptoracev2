/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.dawii.model;

/**
 *
 * @author guilherme
 */
public class RankingEquipe {

    private final int idEquipe;
    private final String nomeEquipe;
    private final int totalDePontos;
    
    public RankingEquipe(int idEquipe, String nomeEquipe, int totalDePontos) {
        this.idEquipe = idEquipe;
        this.nomeEquipe = nomeEquipe;
        this.totalDePontos = totalDePontos;
        
    }

    public int getIdEquipe() {
        return idEquipe;
    }

    public int getTotalDePontos() {
        return totalDePontos;
    }

    public String getNomeEquipe() {
        return nomeEquipe;
    }
}

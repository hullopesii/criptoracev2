package br.dawii.base;

import java.sql.Connection;
import java.sql.Statement;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author tatuapu
 * dados openshift 3
 * database=name criptoRace
 * password gI02kjxeNBHew4O0
 * root pass CWmpASNT8svjHo42
 * dbuser userIE2
 * Username: userCIR Password: DbRPhf5Swt5FSe7E Database Name: sampledb Connection URL: mysql://mysql:3306/
 */
public class ConnectionDAO {
        public static Connection getConnection() throws Exception{
            try{
                Class.forName("com.mysql.jdbc.Driver");
                return DriverManager.getConnection("jdbc:mysql://mysql:3306/criptoRace","userIE2","gI02kjxeNBHew4O0");
                //return DriverManager.getConnection("jdbc:mysql://localhost:3306/criptoRace","admin","admin");
            }catch(ClassNotFoundException | SQLException e){
                throw new Exception(e.getMessage());
            }
        }
        public static void closeConnection(Connection conn, Statement stmt, ResultSet rs) throws Exception{
            close(conn, stmt, rs);
        }
        public static void closeConnection(Connection conn, Statement stmt) throws Exception{
            close(conn, stmt, null);
        }
        public static void closeConnection(Connection conn) throws Exception{
            close(conn,null,null);
        }
        private static void close(Connection conn, Statement stmt, ResultSet rs) throws Exception{
            try{
                if (rs != null) rs.close();
                if (stmt != null) stmt.close();
                if (conn != null) conn.close();
            }catch(Exception e){
                throw new Exception(e.getMessage());
            }
        }
}

package br.dawii.dao;

import br.dawii.base.ConnectionDAO;
import br.dawii.base.DAO;
import br.dawii.model.Candidato;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author tatuapu
 */
public class CandidatoDAO implements DAO {

    private int lastId;

    @Override
    public void atualizar(Object ob) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void excluir(Object ob) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List listaTodos() throws Exception {
        PreparedStatement ps = null;
        Connection conn = null;
        ResultSet rs = null;
        List<Candidato> listaCandidatos = new ArrayList<>();
        try{
            conn = ConnectionDAO.getConnection();
            ps = conn.prepareStatement("SELECT * FROM `view_candidato`");
            rs = ps.executeQuery();
            while(rs.next()){
                listaCandidatos.add(new Candidato(rs.getInt(1),
                        rs.getString(2),rs.getString(3),
                        rs.getString(4), rs.getString(5),
                        rs.getString(6),rs.getInt(7)));
            }
            
        }catch(SQLException sqle){
            throw new Exception(sqle);
        }finally{            
            ConnectionDAO.closeConnection(conn,ps,rs);            
        }
        return listaCandidatos;
    }
    
    public boolean checaSeMatriculaExiste(String mat, int idContest) throws Exception{
        PreparedStatement ps = null;
        Connection conn = null;
        ResultSet rs = null;
        try{
            conn = ConnectionDAO.getConnection();
            ps = conn.prepareStatement("select * from view_candidato WHERE matriculaIFG = ? and idContest=?;");
            ps.setString(1, mat);
            ps.setInt(2, idContest);
            rs = ps.executeQuery();
            while(rs.next()){
                return true;
            }             
        }catch(SQLException sqle){
            throw new Exception(sqle);
        }finally{            
            ConnectionDAO.closeConnection(conn,ps,rs);            
        }
        return false;
    }

    public ArrayList<Candidato> procura(Candidato candidato) throws Exception{
        PreparedStatement ps = null;
        Connection conn = null;
        ResultSet rs = null;
        ArrayList<Candidato> listaCandidatos = new ArrayList<>();
        try{
            conn = ConnectionDAO.getConnection();
            ps = conn.prepareStatement("SELECT * FROM view_candidato WHERE ((idCandidato = ?) or "
                    + "(nomeCompleto = ?) or (nick = ?) or (matriculaIFG=?) or "
                    + "(user = ?) or (passWd = ?)) and idContest=?");
            ps.setInt(1, candidato.getIdCandidato());
            ps.setString(2, candidato.getNomeCompleto());
            ps.setString(3, candidato.getNick());
            ps.setString(4, candidato.getMatriculaIFG());
            ps.setString(5, candidato.getUser());
            ps.setString(6, candidato.getPassWd());
            ps.setInt(7, candidato.getIdContest());
            rs = ps.executeQuery();
            //if(rs.getFetchSize()>0){
            while(rs.next()){
                listaCandidatos.add(new Candidato(rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3), rs.getString(4),
                        rs.getString(6),rs.getString(7),
                        rs.getInt(8)));
            }  
            //}
        }catch(SQLException sqle){
            throw new Exception(sqle);
        }finally{            
            ConnectionDAO.closeConnection(conn,ps,rs);            
        }
        
        return listaCandidatos;
    }
    
    @Override
    public List procura(Object ob) throws Exception {
        Candidato candidato = (Candidato) ob;
        PreparedStatement ps = null;
        Connection conn = null;
        ResultSet rs = null;
        List<Candidato> listaCandidatos = new ArrayList<>();
        try{
            conn = ConnectionDAO.getConnection();
            ps = conn.prepareStatement("SELECT * FROM criptoRace.view_login2 "
                    + "WHERE (user = ? and passWd = ? and idContest=?)");
            ps.setString(1, candidato.getUser());
            ps.setString(2, candidato.getPassWd());
            ps.setInt(3, candidato.getIdContest());
            rs = ps.executeQuery();
            while(rs.next()){
                listaCandidatos.add(new Candidato(rs.getInt(1),
                        rs.getString(2),rs.getString(3),
                        rs.getString(4),
                        rs.getString(6),rs.getString(7),
                        rs.getInt(8)));
            }            
        }catch(SQLException sqle){
            throw new Exception(sqle);
        }finally{            
            ConnectionDAO.closeConnection(conn,ps,rs);            
        }
        return listaCandidatos;
    }

    @Override
    public void salvar(Object ob) throws Exception {
        Candidato candidato = (Candidato) ob;
        PreparedStatement ps = null;
        Connection conn = null;
        if (candidato == null){
            throw new Exception("O valor passado não pode ser nulo!<br>");
        }
        try{
            if(procura(candidato).size()>0)
                throw new Exception("<b>Candidato já existe!</b><br>");
            String SQL = "INSERT INTO `candidato` (`idCandidato`, `nomeCompleto`, `nick`, `matriculaIFG`, `user`, `passWd`, `idContest`) "
                    + "VALUES (NULL,?,?,?,?,?,?);";
            conn = ConnectionDAO.getConnection();
            ps = conn.prepareStatement(SQL,PreparedStatement.RETURN_GENERATED_KEYS);
            ps.setString(1, candidato.getNomeCompleto());
            ps.setString(2, candidato.getNick());
            ps.setString(3, candidato.getMatriculaIFG());
            ps.setString(4, candidato.getUser());
            ps.setString(5, candidato.getPassWd());
            ps.setInt(6, candidato.getIdContest());
            int ret = ps.executeUpdate();
            this.lastId = ret;
            if(ret==0)
               throw new Exception("Erro ao inserir dados do candidato\n Nenhuma linha inseria no BD");
        }catch (SQLException sqle){
            throw new Exception("Erro ao inserir dados do candidato: \n"+sqle);
        }finally{
            ConnectionDAO.closeConnection(conn,ps);
        }
            
    }
    
}

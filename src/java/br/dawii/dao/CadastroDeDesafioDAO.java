/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.dawii.dao;

import br.dawii.base.ConnectionDAO;
import br.dawii.base.DAO;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 *
 * @author guilherme
 */
public class CadastroDeDesafioDAO implements DAO {

    @Override
    public void atualizar(Object ob) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void excluir(Object ob) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List listaTodos() throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List procura(Object ob) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void salvar(Object ob) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public int validarLider(int idCandidato) throws Exception {

        PreparedStatement ps = null;
        Connection conn = null;
        ResultSet rs = null;

        int idEquipe = 0;

        try {
            conn = ConnectionDAO.getConnection();

            ps = conn.prepareStatement("select function_valida_lider(?) ");

            ps.setInt(1, idCandidato);

            rs = ps.executeQuery();
            while (rs.next()) {
                idEquipe = rs.getInt(1);
            }

        } catch (SQLException sqle) {
            throw new Exception(sqle);
        } finally {
            ConnectionDAO.closeConnection(conn, ps);
        }
        return idEquipe;

    }

    public int insereDesafio(String descricao, int idCandidato, int pontuacao, int idEquipe) throws Exception {

        PreparedStatement ps = null;
        Connection conn = null;
        ResultSet rs = null;

        int idDesafio = 0;

        try {
            conn = ConnectionDAO.getConnection();

            ps = conn.prepareStatement("select function_cadastra_desafio_equipe(?, ?, ?, ?)");

            ps.setString(1, descricao);
            ps.setInt(2, idCandidato);
            ps.setInt(3, pontuacao);
            ps.setInt(4, idEquipe);

            rs = ps.executeQuery();
            while (rs.next()) {
                idDesafio = rs.getInt(1);
            }

        } catch (SQLException sqle) {
            throw new Exception(sqle);
        } finally {
            ConnectionDAO.closeConnection(conn, ps);
        }

        return idDesafio;
    }

}

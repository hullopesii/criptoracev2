/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.dawii.dao;

import br.dawii.base.ConnectionDAO;
import br.dawii.base.DAO;
import br.dawii.model.*;
//import br.dawii.model.RankingEquipe;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author rafael
 */
public class RankingDAO implements DAO {

    @Override
    public void atualizar(Object ob) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void excluir(Object ob) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List listaTodos() throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List procura(Object ob) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void salvar(Object ob) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    public List<RankingGeral> buscaRankingGeralPositivo() throws Exception{
        
        
        PreparedStatement ps = null;
        Connection conn = null;
        ResultSet rs = null;
        List<RankingGeral> listaRanking = new ArrayList<>();
        try {
            conn = ConnectionDAO.getConnection();
            ps = conn.prepareStatement("SELECT * FROM `view_ranking` order by totalPontos desc LIMIT 0,10 ");
            rs = ps.executeQuery();
            while (rs.next()) {
                listaRanking.add(new RankingGeral(rs.getInt(1), rs.getString(2), rs.getInt(4)));
            }

        } catch (SQLException sqle) {
            throw new Exception(sqle);
        } finally {
            ConnectionDAO.closeConnection(conn, ps, rs);
        }
        return listaRanking;
    }
    public List<RankingGeral> buscaRankingGeralNegativo() throws Exception{
        
        PreparedStatement ps = null;
        Connection conn = null;
        ResultSet rs = null;
        List<RankingGeral> listaRanking = new ArrayList<>();
        try {
            conn = ConnectionDAO.getConnection();
            ps = conn.prepareStatement("SELECT * FROM `view_ranking_negativo`");
            rs = ps.executeQuery();
            while (rs.next()) {
                listaRanking.add(new RankingGeral(rs.getInt(1), rs.getString(2), rs.getInt(3)));
            }

        } catch (SQLException sqle) {
            throw new Exception(sqle);
        } finally {
            ConnectionDAO.closeConnection(conn, ps, rs);
        }
        return listaRanking;
    }
    
    public List<RankingEquipe> buscaRankingEquipe() throws Exception{
        
        
        PreparedStatement ps = null;
        Connection conn = null;
        ResultSet rs = null;
        List<RankingEquipe> listaRanking = new ArrayList<>();
        try {
            conn = ConnectionDAO.getConnection();
            ps = conn.prepareStatement("SELECT * FROM `view_ranking` order by totalPontos desc LIMIT 0,10 ");
            rs = ps.executeQuery();
            while (rs.next()) {
                listaRanking.add(new RankingEquipe(rs.getInt(1), rs.getString(2), rs.getInt(4)));
            }

        } catch (SQLException sqle) {
            throw new Exception(sqle);
        } finally {
            ConnectionDAO.closeConnection(conn, ps, rs);
        }
        return listaRanking;
    }
    
    public List<RankingIndividual> buscaRankingIndividual(int id) throws Exception{
        
        PreparedStatement ps = null;
        Connection conn = null;
        ResultSet rs = null;
        List<RankingIndividual> listaRanking = new ArrayList<>();
        try {
            conn = ConnectionDAO.getConnection();
            ps = conn.prepareStatement(" select * from view_ranking_individual where id = ?");
            ps.setInt(1, id);
            rs = ps.executeQuery();
            while (rs.next()) {
                listaRanking.add(new RankingIndividual(rs.getInt(4), rs.getInt(5), rs.getInt(6)));
            }

        } catch (SQLException sqle) {
            throw new Exception(sqle);
        } finally {
            ConnectionDAO.closeConnection(conn, ps, rs);
        }
        return listaRanking;
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.dawii.dao;

import br.dawii.base.ConnectionDAO;
import br.dawii.base.DAO;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 *
 * @author guilherme
 */
public class CadastroDeEquipesDAO implements DAO {

    @Override
    public void atualizar(Object ob) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void excluir(Object ob) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List listaTodos() throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List procura(Object ob) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void salvar(Object ob) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public int insereEquipe(String nomeEquipe) throws Exception {

        PreparedStatement ps = null;
        Connection conn = null;
        ResultSet rs = null;

        int idEquipe = 0;

        try {
            conn = ConnectionDAO.getConnection();

            ps = conn.prepareStatement("select function_cadastra_equipe(?, ?)");

            ps.setString(1, nomeEquipe);
            ps.setInt(2, 1);

            rs = ps.executeQuery();
            while (rs.next()) {
                idEquipe = rs.getInt(1);
            }

        } catch (SQLException sqle) {
            throw new Exception(sqle);
        } finally {
            ConnectionDAO.closeConnection(conn, ps);
        }

        return idEquipe;
    }

    public int associarCandidatoEquipe(int idEquipe, int idCandidato, String tipo) throws Exception {
        PreparedStatement ps = null;
        Connection conn = null;
        ResultSet rs = null;

        int resultado = 0;
        try {
            conn = ConnectionDAO.getConnection();

            ps = conn.prepareStatement("select function_associa_candidato_equipe(?, ?, ?)");

            ps.setInt(1, idEquipe);
            ps.setInt(2, idCandidato);
            ps.setString(3, tipo);

            rs = ps.executeQuery();
            while (rs.next()) {
                resultado = rs.getInt(1);
            }

        } catch (SQLException sqle) {
            throw new Exception(sqle);
        } finally {
            ConnectionDAO.closeConnection(conn, ps);
        }
        return resultado;
    }

    public int buscaEquipeDoCandidato(int idCandidato) throws Exception {

        PreparedStatement ps = null;
        Connection conn = null;
        ResultSet rs = null;

        int idEquipe = 0;

        try {
            conn = ConnectionDAO.getConnection();

            ps = conn.prepareStatement("select equipe_idEquipe from equipe_has_candidato where candidato_idCandidato = ? ");

            ps.setInt(1, idCandidato);

            rs = ps.executeQuery();
            while (rs.next()) {
                idEquipe = rs.getInt(1);
            }

        } catch (SQLException sqle) {
            throw new Exception(sqle);
        } finally {
            ConnectionDAO.closeConnection(conn, ps);
        }
        return idEquipe;
    }

    public String buscaNomeEquipeDoCandidato(int idCandidato) throws Exception {

        PreparedStatement ps = null;
        Connection conn = null;
        ResultSet rs = null;

        String nomeEquipe = null;

        try {
            conn = ConnectionDAO.getConnection();

            ps = conn.prepareStatement("SELECT  nomeEquipe FROM equipe, equipe_has_candidato, candidato "
                    + "WHERE equipe_has_candidato.candidato_idCandidato = ? and equipe.idEquipe = equipe_has_candidato.equipe_idEquipe LIMIT 0,1");

            ps.setInt(1, idCandidato);

            rs = ps.executeQuery();
            while (rs.next()) {
                nomeEquipe = rs.getString(1);
            }

        } catch (SQLException sqle) {
            throw new Exception(sqle);
        } finally {
            ConnectionDAO.closeConnection(conn, ps);
        }
        return nomeEquipe;
    }

}

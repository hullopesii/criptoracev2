/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.dawii.dao;

import br.dawii.base.ConnectionDAO;
import br.dawii.base.DAO;
import br.dawii.model.Mensagem;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author rafael
 */
public class MensagensDAO implements DAO {
    private int idContest;
    
    public MensagensDAO(int idC){
        this.idContest = idC;
    }

    @Override
    public void atualizar(Object ob) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void excluir(Object ob) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List listaTodos() throws Exception {
        
        PreparedStatement ps = null;
        Connection conn = null;
        ResultSet rs = null;
        
        List<Mensagem> mensagens = new ArrayList();
        
        try {
            conn = ConnectionDAO.getConnection();
            
            ps = conn.prepareStatement("SELECT * FROM `view_mensagens` where idContest = ?");
            ps.setInt(1, this.idContest);
            rs = ps.executeQuery();
            while (rs.next()) {
                mensagens.add(new Mensagem(rs.getString(1),rs.getInt(2)));
            }

        } catch (SQLException sqle) {
            throw new Exception(sqle);
        } finally {
            ConnectionDAO.closeConnection(conn, ps, rs);
        }
        return mensagens;
    }

    @Override
    public List procura(Object ob) throws Exception {
        
        int idCandidato = (int) ob;
        
        PreparedStatement ps = null;
        Connection conn = null;
        ResultSet rs = null;
        
        List<Mensagem> mensagens = new ArrayList();
        
        try {
            conn = ConnectionDAO.getConnection();
            
            ps = conn.prepareStatement("SELECT mensagem, nick, candidato.idCandidato, idMensagens FROM candidato, mensagens where mensagens.idCandidato = candidato.idCandidato and candidato.idCandidato = ?");
           ps.setInt(1, idCandidato);
            rs = ps.executeQuery();
            while (rs.next()) {
                mensagens.add(new Mensagem(rs.getString(1), rs.getString(2), rs.getInt(3), rs.getInt(4)));
            }

        } catch (SQLException sqle) {
            throw new Exception(sqle);
        } finally {
            ConnectionDAO.closeConnection(conn, ps, rs);
        }
        return mensagens;
        
    }

    @Override
    public void salvar(Object ob) throws Exception {
        Mensagem mensagem = (Mensagem) ob;
        
        PreparedStatement ps = null;
        Connection conn = null;
        ResultSet rs = null;
        
        try {
            conn = ConnectionDAO.getConnection();

            ps = conn.prepareStatement("select function_salva_msg(?,?)");
            ps.setString(1, mensagem.getMsg());
            ps.setInt(2, mensagem.getIdCandidato());
            
            rs = ps.executeQuery();
            
        }catch (SQLException sqle) {
            throw new Exception(sqle);
        } finally {
            ConnectionDAO.closeConnection(conn, ps);
        }
    }
    
}

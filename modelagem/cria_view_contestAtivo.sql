/*uso: select * from view_contestAtivo : retorna só o último ativo*/

CREATE 
VIEW `criptoRace`.`view_contestAtivo` AS
    SELECT 
        `criptoRace`.`contest`.`idContest` AS `idContest`,
        `criptoRace`.`contest`.`nomeContest` AS `nomeContest`,
        `criptoRace`.`contest`.`status` AS `status`,
        `criptoRace`.`contest`.`loc` AS `loc`
    FROM
        `criptoRace`.`contest`
    WHERE
        (`criptoRace`.`contest`.`status` = 'Ativo')
    ORDER BY `criptoRace`.`contest`.`idContest` DESC
    LIMIT 0 , 1

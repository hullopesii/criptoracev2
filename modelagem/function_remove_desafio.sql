CREATE DEFINER=`root`@`localhost` FUNCTION `function_remove_desafio`(
	idD int(11)) RETURNS int(11)
BEGIN
	if (canDeleteDesafio(idD))then
		delete from desafios where idDesafio = idD;
        RETURN ROW_COUNT();
    else    
		RETURN -1;
    end if;    
END
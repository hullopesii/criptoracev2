CREATE FUNCTION `pegaIdDesafio`(resp longtext, idC int(11)) RETURNS int(11)
BEGIN
DECLARE idD int(11);
    set idD = (
			select desafios.idDesafio from respDesafio inner join desafios on desafios.idDesafio = respDesafio.idDesafio
			where (desafios.status= "Ativo" and respDesafio.resposta = resp and desafios.idContest = idC) Limit 1
            );
RETURN idD;
END
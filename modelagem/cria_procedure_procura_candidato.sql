CREATE DEFINER=`root`@`localhost` PROCEDURE `procedure_procura_candidato`(termoBusca varchar(400), idC int(11))
BEGIN
	SELECT * FROM `candidato` WHERE ((nomeCompleto like CONCAT('%',termoBusca,'%')) or (nick like CONCAT('%',termoBusca,'%')) or (matriculaIFG like CONCAT('%',termoBusca,'%')) or (cpf like CONCAT('%',termoBusca,'%')) and (idContest=idC));
END
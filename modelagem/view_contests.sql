CREATE 
VIEW `criptoRace`.`view_contests` AS
    SELECT 
        `criptoRace`.`desafios`.`idDesafio` AS `idDesafio`,
        `criptoRace`.`desafios`.`descricaoDesafio` AS `descricaoDesafio`,
        `criptoRace`.`desafios`.`idContest` AS `idContest`,
        `criptoRace`.`desafios`.`status` AS `status`,
        `criptoRace`.`desafios`.`pontuacao` AS `pontuacao`,
        `criptoRace`.`desafios`.`tipoDesafio` AS `tipoDesafio`,
        `criptoRace`.`desafios`.`idEquipe` AS `idEquipe`
    FROM
        `criptoRace`.`desafios`
    ORDER BY `criptoRace`.`desafios`.`idDesafio` DESC
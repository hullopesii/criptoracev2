-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Sep 28, 2017 at 09:30 AM
-- Server version: 5.7.19-0ubuntu0.16.04.1
-- PHP Version: 7.0.18-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `criptoRace`
--

DELIMITER $$
--
-- Procedures
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `procedure_cadastraCandidato` (IN `nC` VARCHAR(100), IN `nick` VARCHAR(100), IN `matricula` VARCHAR(30), IN `cpf` VARCHAR(15), IN `usuario` VARCHAR(45), IN `pass` VARCHAR(200), IN `idC` INT, OUT `inserido` INT)  BEGIN
	INSERT INTO `candidato`
		(nomeCompleto,nick,matriculaIFG,cpf,user,passWd,idContest)
        values
        (nC,nick,matricula,cpf,usuario,pass,idC);
        SET inserido = LAST_INSERT_ID();
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `procedure_inativaDesafio` (`idD` INT(11))  BEGIN
	update desafios set status = "Inativo" where idDesafio = idD;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `procedure_view_respostas_desafio` (`idD` INT(11))  BEGIN
	select * from respDesafio where idDesafio = idD;
END$$

--
-- Functions
--
CREATE DEFINER=`root`@`localhost` FUNCTION `canDeleteDesafio` (`idD` INT(11)) RETURNS TINYINT(1) BEGIN
	DECLARE c1 int(11);
    DECLARE c2 int(11);
    DECLARE c3 int(11);
    set c1 = (select count(*) from acertoCandidato where idDesafio = idD);
    set c2 = (select count(*) from respostaSubmetida where idDesafio = idD);
    set c3 = (select count(*) from respDesafio where idDesafio = idD);
	if ((c1>0)or (c2>0) or (c3>0))then
		return false;
	end if;
		RETURN true;
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `function_altera_descricao_desafio` (`novaDescricao` VARCHAR(300), `idD` INT(11)) RETURNS INT(11) BEGIN
	update desafios set descricaoDesafio = novaDescricao where idDesafio = idD;
RETURN ROW_COUNT();
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `function_altera_status_desafio` (`novoStatus` VARCHAR(20), `idD` INT(11)) RETURNS INT(11) BEGIN
	if(novoStatus = "Ativo" or novoStatus = "Inativo") then
		update desafios set status = novoStatus where idDesafio = idD;
        RETURN ROW_COUNT();
    else
		return 0;
    end if;
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `function_altera_tipo_desafio` (`novoTipo` VARCHAR(30), `idD` INT(11)) RETURNS INT(11) BEGIN
if(novoTipo = "Individual" or novoTipo = "Equipe") then
	update desafios set tipoDesafio = novoTipo where idDesafio = idD;
    return ROW_COUNT();
else
		return 0;
end if;
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `function_cadastraAdm` (`nC` VARCHAR(100), `nick` VARCHAR(100), `matricula` VARCHAR(30), `cpf` VARCHAR(15), `usuario` VARCHAR(400), `pass` VARCHAR(400), `uL` VARCHAR(400), `pL` VARCHAR(400)) RETURNS INT(11) BEGIN
	if(hasAdm(uL,pL)>0)then
        if((SELECT count(*) FROM administrador WHERE (user = usuario))=0)then
			INSERT INTO `administrador`
				(nomeCompleto,nick,matriculaIFG,cpf,user,passWd)
				values
				(nC,nick,matricula,cpf,usuario,pass);
				return LAST_INSERT_ID();
        else
			return -1;
        end if;    
     else
			return 0;
     end if;   
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `function_cadastraCandidato` (`nC` VARCHAR(100), `nick` VARCHAR(100), `matricula` VARCHAR(30), `cpf` VARCHAR(15), `usuario` VARCHAR(45), `pass` VARCHAR(200), `idC` INT) RETURNS INT(11) BEGIN
if((select count(*) from candidato where (matriculaIFG=matricula))=0)then
	if((select count(*) from candidato where (user=usuario))=0)then
		INSERT INTO `candidato`
		(nomeCompleto,nick,matriculaIFG,cpf,user,passWd,idContest)
		values
		(nC,nick,matricula,cpf,usuario,pass,idC);
		return LAST_INSERT_ID();
	else
		return 0;
	end if;
else
	return 0;
end if; 
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `function_cadastra_desafio` (`descD` VARCHAR(300), `idC` INT(11), `pontuacao` INT(10), `tpDesafio` VARCHAR(20), `idEquipe` INT(11)) RETURNS INT(11) BEGIN
	insert into desafios values
    (null,
	descD,
	idC,
    "Inativo",
	pontuacao,
	tpDesafio,
	idEquipe);
RETURN LAST_INSERT_ID();
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `function_cadastra_respDesafio` (`nResp` LONGTEXT, `idD` INT(11)) RETURNS INT(11) BEGIN
	insert into respDesafio values (null, nResp, idD);
RETURN LAST_INSERT_ID();
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `function_remove_desafio` (`idD` INT(11)) RETURNS INT(11) BEGIN
	if (canDeleteDesafio(idD))then
		delete from desafios where idDesafio = idD;
        RETURN ROW_COUNT();
    else    
		RETURN -1;
    end if;    
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `function_valida_resposta` (`resp` LONGTEXT, `idC` INT(11)) RETURNS INT(11) BEGIN	
 DECLARE pontos int(11);
 DECLARE idD int(11);
 
 set pontos = (pegaPontuacao(resp, idC));
  
 if(pontos <> null or pontos > 0)then
	set idD = (pegaIdDesafio(resp, idC));
	if(isInactively(idD))then		
        /*update desafios set status = "Inativo" where idDesafio = idD;*/
        call procedure_inativaDesafio(idD);
    end if;
	RETURN pontos;
 else
	RETURN 0;
 end if;

END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `hasAdm` (`u` VARCHAR(400), `s` VARCHAR(400)) RETURNS INT(11) BEGIN
	return (select count(*) from administrador where (user=u and passWd=s));

END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `inativaDesafio` (`idD` INT(11)) RETURNS INT(11) BEGIN	
	update desafios set status = "Inativo" where idDesafio = idD;
RETURN 1;
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `isInactively` (`idD` INT(11)) RETURNS TINYINT(1) BEGIN
	if ((select count(*) from inativos where idDesafio = idD)>0)then
		return true;
    else
		return false;
    end if;
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `new_function` (`i` INT) RETURNS INT(11) BEGIN

RETURN i;
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `pegaIdDesafio` (`resp` LONGTEXT, `idC` INT(11)) RETURNS INT(11) BEGIN
DECLARE idD int(11);
    set idD = (
			select desafios.idDesafio from respDesafio inner join desafios on desafios.idDesafio = respDesafio.idDesafio
			where (desafios.status= "Ativo" and respDesafio.resposta = resp and desafios.idContest = idC) Limit 1
            );
RETURN idD;
END$$

CREATE DEFINER=`root`@`localhost` FUNCTION `pegaPontuacao` (`resp` LONGTEXT, `idC` INT(11)) RETURNS INT(11) BEGIN
	DECLARE pontuacao int(11);
    set pontuacao =(select desafios.pontuacao from respDesafio inner join desafios on desafios.idDesafio = respDesafio.idDesafio
		where (desafios.status= "Ativo" and respDesafio.resposta = resp and desafios.idContest = idC) Limit 1);
RETURN pontuacao;
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `acertoCandidato`
--

CREATE TABLE `acertoCandidato` (
  `idAcertos` int(11) NOT NULL,
  `idDesafio` int(11) DEFAULT NULL,
  `idRespostaSubmetida` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `acertoCandidato`
--

INSERT INTO `acertoCandidato` (`idAcertos`, `idDesafio`, `idRespostaSubmetida`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 2, 3);

-- --------------------------------------------------------

--
-- Table structure for table `administrador`
--

CREATE TABLE `administrador` (
  `idCandidato` int(11) NOT NULL,
  `nomeCompleto` varchar(100) NOT NULL,
  `nick` varchar(100) DEFAULT NULL,
  `matriculaIFG` varchar(30) DEFAULT NULL,
  `cpf` varchar(15) DEFAULT NULL,
  `user` varchar(400) NOT NULL,
  `passWd` varchar(400) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `administrador`
--

INSERT INTO `administrador` (`idCandidato`, `nomeCompleto`, `nick`, `matriculaIFG`, `cpf`, `user`, `passWd`) VALUES
(1, 'Tester', 'testador', '000', '', 'admin', 'admin'),
(2, 'TESTE2', 'TESTE2', '0', '44', 'teste', 'teste'),
(5, 'TESTE2', 'TESTE2', '03', '44', 'testse', 'teste');

-- --------------------------------------------------------

--
-- Table structure for table `candidato`
--

CREATE TABLE `candidato` (
  `idCandidato` int(11) NOT NULL,
  `nomeCompleto` varchar(100) NOT NULL,
  `nick` varchar(100) DEFAULT NULL,
  `matriculaIFG` varchar(30) DEFAULT NULL,
  `cpf` varchar(15) DEFAULT NULL,
  `user` varchar(400) DEFAULT NULL,
  `passWd` varchar(400) DEFAULT NULL,
  `idContest` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `candidato`
--

INSERT INTO `candidato` (`idCandidato`, `nomeCompleto`, `nick`, `matriculaIFG`, `cpf`, `user`, `passWd`, `idContest`) VALUES
(1, 'usuário teste', 'tester', '99999999', NULL, 'tester', 'tester', 1),
(2, 'Usuário teste 2', 'tester2', '888', NULL, 'tester2', 'tester2', 1),
(3, 'Usuário teste 3', 'tester3', '4444', NULL, 'tester3', 'tester3', 1),
(4, 'victor', 'vitalino', '00', '00', 'victor', 'victor', 1),
(6, 'victor', 'vitalino', '1', '00', 'victor', 'victor', 1),
(7, 'fulano', 'eita', '99', '7', 'fulano', 'fulano', 1),
(8, 'fulano', 'eita', '996', '7', 'fulano', 'fulano', 1),
(9, 'fulano de TAl', 'fulanex', '', '', 'fulano', '123', 1);

-- --------------------------------------------------------

--
-- Table structure for table `contest`
--

CREATE TABLE `contest` (
  `idContest` int(11) NOT NULL,
  `nomeContest` varchar(200) NOT NULL,
  `status` enum('Ativo','Inativo') DEFAULT NULL,
  `loc` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `contest`
--

INSERT INTO `contest` (`idContest`, `nomeContest`, `status`, `loc`) VALUES
(1, 'teste', 'Ativo', 'ifg-inhumas');

-- --------------------------------------------------------

--
-- Table structure for table `desafios`
--

CREATE TABLE `desafios` (
  `idDesafio` int(11) NOT NULL,
  `descricaoDesafio` varchar(300) NOT NULL,
  `idContest` int(11) NOT NULL,
  `status` enum('Ativo','Inativo') NOT NULL,
  `pontuacao` int(1) NOT NULL,
  `tipoDesafio` enum('Individual','Equipe') DEFAULT NULL,
  `idEquipe` int(11) DEFAULT NULL COMMENT 'Id da equipe criadora, para o caso de desafios criados por equipes. Caso seja criado pelo adm, fica em branco.\n'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `desafios`
--

INSERT INTO `desafios` (`idDesafio`, `descricaoDesafio`, `idContest`, `status`, `pontuacao`, `tipoDesafio`, `idEquipe`) VALUES
(1, 'teste', 1, 'Ativo', 1, 'Individual', NULL),
(2, 'novo desafio', 1, 'Ativo', 2, 'Individual', NULL),
(5, 'testando cadastro via function', 1, 'Inativo', 10, 'Individual', 0);

-- --------------------------------------------------------

--
-- Table structure for table `equipe`
--

CREATE TABLE `equipe` (
  `idEquipe` int(11) NOT NULL,
  `nomeEquipe` varchar(45) NOT NULL,
  `idContest` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `equipe`
--

INSERT INTO `equipe` (`idEquipe`, `nomeEquipe`, `idContest`) VALUES
(1, 'Testadores', 1);

-- --------------------------------------------------------

--
-- Table structure for table `equipe_has_candidato`
--

CREATE TABLE `equipe_has_candidato` (
  `equipe_idEquipe` int(11) NOT NULL,
  `candidato_idCandidato` int(11) NOT NULL,
  `tipo` enum('lider','membro') DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='representa  participação de um candidato em uma equipe, em um contest';

--
-- Dumping data for table `equipe_has_candidato`
--

INSERT INTO `equipe_has_candidato` (`equipe_idEquipe`, `candidato_idCandidato`, `tipo`) VALUES
(1, 1, 'lider'),
(1, 2, 'membro');

-- --------------------------------------------------------

--
-- Table structure for table `inativos`
--

CREATE TABLE `inativos` (
  `idInativos` int(11) NOT NULL,
  `idDesafio` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `inativos`
--

INSERT INTO `inativos` (`idInativos`, `idDesafio`) VALUES
(1, 5);

-- --------------------------------------------------------

--
-- Table structure for table `respDesafio`
--

CREATE TABLE `respDesafio` (
  `idResp` int(11) NOT NULL,
  `resposta` longtext NOT NULL,
  `idDesafio` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `respDesafio`
--

INSERT INTO `respDesafio` (`idResp`, `resposta`, `idDesafio`) VALUES
(1, 'teste', 1),
(2, 'teste2', 2),
(3, 'nova resposta enviada', 1),
(4, 'indio', 5);

-- --------------------------------------------------------

--
-- Table structure for table `respostaSubmetida`
--

CREATE TABLE `respostaSubmetida` (
  `idRespSubmetida` int(11) NOT NULL,
  `respostaSubmetida` longtext,
  `idCandidato` int(11) DEFAULT NULL,
  `idDesafio` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `respostaSubmetida`
--

INSERT INTO `respostaSubmetida` (`idRespSubmetida`, `respostaSubmetida`, `idCandidato`, `idDesafio`) VALUES
(1, 'teste', 1, 1),
(2, 'teste2', 2, 1),
(3, 'teste2', 1, 2),
(4, 'ual', 1, 1),
(5, 'ual', 1, 2),
(6, 'ual', 1, 2),
(7, 'ual', 1, 2);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_candidato`
--
CREATE TABLE `view_candidato` (
`idCandidato` int(11)
,`nomeCompleto` varchar(100)
,`nick` varchar(100)
,`matriculaIFG` varchar(30)
,`cpf` varchar(15)
,`user` varchar(400)
,`passWd` varchar(400)
,`idContest` int(11)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_contestAtivo`
--
CREATE TABLE `view_contestAtivo` (
`idContest` int(11)
,`nomeContest` varchar(200)
,`status` enum('Ativo','Inativo')
,`loc` varchar(100)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_desafios`
--
CREATE TABLE `view_desafios` (
`idDesafio` int(11)
,`descricaoDesafio` varchar(300)
,`idContest` int(11)
,`status` enum('Ativo','Inativo')
,`pontuacao` int(1)
,`tipoDesafio` enum('Individual','Equipe')
,`idEquipe` int(11)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_desafios_completo`
--
CREATE TABLE `view_desafios_completo` (
`idDesafio` int(11)
,`descricaoDesafio` varchar(300)
,`status` enum('Ativo','Inativo')
,`pontuacao` int(1)
,`tipoDesafio` enum('Individual','Equipe')
,`idEquipe` int(11)
,`certas` bigint(21)
,`todas` bigint(21)
,`idContest` int(11)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_login`
--
CREATE TABLE `view_login` (
`idCandidato` int(11)
,`nomeCompleto` varchar(100)
,`nick` varchar(100)
,`matriculaIFG` varchar(30)
,`cpf` varchar(15)
,`user` varchar(400)
,`passWd` varchar(400)
,`idContest` int(11)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_login2`
--
CREATE TABLE `view_login2` (
`idCandidato` int(11)
,`nomeCompleto` varchar(100)
,`nick` varchar(100)
,`matriculaIFG` varchar(30)
,`cpf` varchar(15)
,`user` varchar(400)
,`passWd` varchar(400)
,`idContest` int(11)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_login_adm`
--
CREATE TABLE `view_login_adm` (
`idCandidato` int(11)
,`nomeCompleto` varchar(100)
,`nick` varchar(100)
,`matriculaIFG` varchar(30)
,`cpf` varchar(15)
,`user` varchar(400)
,`passWd` varchar(400)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_ranking`
--
CREATE TABLE `view_ranking` (
`id` int(11)
,`nick` varchar(100)
,`nomeCompleto` varchar(100)
,`totalPontos` decimal(32,0)
);

-- --------------------------------------------------------

--
-- Structure for view `view_candidato`
--
DROP TABLE IF EXISTS `view_candidato`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_candidato`  AS  select `candidato`.`idCandidato` AS `idCandidato`,`candidato`.`nomeCompleto` AS `nomeCompleto`,`candidato`.`nick` AS `nick`,`candidato`.`matriculaIFG` AS `matriculaIFG`,`candidato`.`cpf` AS `cpf`,`candidato`.`user` AS `user`,`candidato`.`passWd` AS `passWd`,`candidato`.`idContest` AS `idContest` from `candidato` ;

-- --------------------------------------------------------

--
-- Structure for view `view_contestAtivo`
--
DROP TABLE IF EXISTS `view_contestAtivo`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_contestAtivo`  AS  select `contest`.`idContest` AS `idContest`,`contest`.`nomeContest` AS `nomeContest`,`contest`.`status` AS `status`,`contest`.`loc` AS `loc` from `contest` where (`contest`.`status` = 'Ativo') order by `contest`.`idContest` desc limit 0,1 ;

-- --------------------------------------------------------

--
-- Structure for view `view_desafios`
--
DROP TABLE IF EXISTS `view_desafios`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_desafios`  AS  select `desafios`.`idDesafio` AS `idDesafio`,`desafios`.`descricaoDesafio` AS `descricaoDesafio`,`desafios`.`idContest` AS `idContest`,`desafios`.`status` AS `status`,`desafios`.`pontuacao` AS `pontuacao`,`desafios`.`tipoDesafio` AS `tipoDesafio`,`desafios`.`idEquipe` AS `idEquipe` from `desafios` ;

-- --------------------------------------------------------

--
-- Structure for view `view_desafios_completo`
--
DROP TABLE IF EXISTS `view_desafios_completo`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_desafios_completo`  AS  select `desafios`.`idDesafio` AS `idDesafio`,`desafios`.`descricaoDesafio` AS `descricaoDesafio`,`desafios`.`status` AS `status`,`desafios`.`pontuacao` AS `pontuacao`,`desafios`.`tipoDesafio` AS `tipoDesafio`,`desafios`.`idEquipe` AS `idEquipe`,(select count(0) from `acertoCandidato` where (`acertoCandidato`.`idDesafio` = `desafios`.`idDesafio`)) AS `certas`,(select count(0) from `respostaSubmetida` where (`respostaSubmetida`.`idDesafio` = `desafios`.`idDesafio`)) AS `todas`,`desafios`.`idContest` AS `idContest` from `desafios` ;

-- --------------------------------------------------------

--
-- Structure for view `view_login`
--
DROP TABLE IF EXISTS `view_login`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_login`  AS  select `candidato`.`idCandidato` AS `idCandidato`,`candidato`.`nomeCompleto` AS `nomeCompleto`,`candidato`.`nick` AS `nick`,`candidato`.`matriculaIFG` AS `matriculaIFG`,`candidato`.`cpf` AS `cpf`,`candidato`.`user` AS `user`,`candidato`.`passWd` AS `passWd`,`candidato`.`idContest` AS `idContest` from `candidato` limit 0,1 ;

-- --------------------------------------------------------

--
-- Structure for view `view_login2`
--
DROP TABLE IF EXISTS `view_login2`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_login2`  AS  select `candidato`.`idCandidato` AS `idCandidato`,`candidato`.`nomeCompleto` AS `nomeCompleto`,`candidato`.`nick` AS `nick`,`candidato`.`matriculaIFG` AS `matriculaIFG`,`candidato`.`cpf` AS `cpf`,`candidato`.`user` AS `user`,`candidato`.`passWd` AS `passWd`,`candidato`.`idContest` AS `idContest` from `candidato` ;

-- --------------------------------------------------------

--
-- Structure for view `view_login_adm`
--
DROP TABLE IF EXISTS `view_login_adm`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_login_adm`  AS  select `administrador`.`idCandidato` AS `idCandidato`,`administrador`.`nomeCompleto` AS `nomeCompleto`,`administrador`.`nick` AS `nick`,`administrador`.`matriculaIFG` AS `matriculaIFG`,`administrador`.`cpf` AS `cpf`,`administrador`.`user` AS `user`,`administrador`.`passWd` AS `passWd` from `administrador` ;

-- --------------------------------------------------------

--
-- Structure for view `view_ranking`
--
DROP TABLE IF EXISTS `view_ranking`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_ranking`  AS  select `candidato`.`idCandidato` AS `id`,`candidato`.`nick` AS `nick`,`candidato`.`nomeCompleto` AS `nomeCompleto`,sum(`desafios`.`pontuacao`) AS `totalPontos` from (((`acertoCandidato` join `respostaSubmetida` on((`respostaSubmetida`.`idRespSubmetida` = `acertoCandidato`.`idRespostaSubmetida`))) join `desafios` on((`desafios`.`idDesafio` = `acertoCandidato`.`idDesafio`))) join `candidato` on((`candidato`.`idCandidato` = `respostaSubmetida`.`idCandidato`))) group by `candidato`.`idCandidato` order by `totalPontos` desc ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `acertoCandidato`
--
ALTER TABLE `acertoCandidato`
  ADD PRIMARY KEY (`idAcertos`),
  ADD KEY `fk_acertoCandidato_desafios` (`idDesafio`),
  ADD KEY `fk_acertoCandidato_respostaSubmetida` (`idRespostaSubmetida`);

--
-- Indexes for table `administrador`
--
ALTER TABLE `administrador`
  ADD PRIMARY KEY (`idCandidato`),
  ADD UNIQUE KEY `user_UNIQUE` (`user`),
  ADD UNIQUE KEY `matriculaIFG` (`matriculaIFG`),
  ADD UNIQUE KEY `matriculaIFG_2` (`matriculaIFG`),
  ADD KEY `cpf_2` (`cpf`);

--
-- Indexes for table `candidato`
--
ALTER TABLE `candidato`
  ADD PRIMARY KEY (`idCandidato`),
  ADD UNIQUE KEY `matriculaIFG` (`matriculaIFG`),
  ADD UNIQUE KEY `matriculaIFG_2` (`matriculaIFG`),
  ADD KEY `cpf_2` (`cpf`),
  ADD KEY `fk_candidato_contest1_idx` (`idContest`);

--
-- Indexes for table `contest`
--
ALTER TABLE `contest`
  ADD PRIMARY KEY (`idContest`);

--
-- Indexes for table `desafios`
--
ALTER TABLE `desafios`
  ADD PRIMARY KEY (`idDesafio`),
  ADD KEY `fk_desafios_contest` (`idContest`);

--
-- Indexes for table `equipe`
--
ALTER TABLE `equipe`
  ADD PRIMARY KEY (`idEquipe`),
  ADD KEY `fk_equipe_contest1_idx` (`idContest`);

--
-- Indexes for table `equipe_has_candidato`
--
ALTER TABLE `equipe_has_candidato`
  ADD PRIMARY KEY (`equipe_idEquipe`,`candidato_idCandidato`),
  ADD KEY `fk_equipe_has_candidato_candidato1_idx` (`candidato_idCandidato`),
  ADD KEY `fk_equipe_has_candidato_equipe1_idx` (`equipe_idEquipe`);

--
-- Indexes for table `inativos`
--
ALTER TABLE `inativos`
  ADD PRIMARY KEY (`idInativos`),
  ADD KEY `fk_inativos_desafios` (`idDesafio`);

--
-- Indexes for table `respDesafio`
--
ALTER TABLE `respDesafio`
  ADD PRIMARY KEY (`idResp`),
  ADD KEY `fk_respDesafio_desafios` (`idDesafio`);

--
-- Indexes for table `respostaSubmetida`
--
ALTER TABLE `respostaSubmetida`
  ADD PRIMARY KEY (`idRespSubmetida`),
  ADD KEY `fk_respostaSubmetida_candidato` (`idCandidato`),
  ADD KEY `fk_respostaSubmetida_desafios` (`idDesafio`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `acertoCandidato`
--
ALTER TABLE `acertoCandidato`
  MODIFY `idAcertos` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `administrador`
--
ALTER TABLE `administrador`
  MODIFY `idCandidato` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `candidato`
--
ALTER TABLE `candidato`
  MODIFY `idCandidato` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `contest`
--
ALTER TABLE `contest`
  MODIFY `idContest` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `desafios`
--
ALTER TABLE `desafios`
  MODIFY `idDesafio` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `equipe`
--
ALTER TABLE `equipe`
  MODIFY `idEquipe` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `inativos`
--
ALTER TABLE `inativos`
  MODIFY `idInativos` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `respDesafio`
--
ALTER TABLE `respDesafio`
  MODIFY `idResp` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `respostaSubmetida`
--
ALTER TABLE `respostaSubmetida`
  MODIFY `idRespSubmetida` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `acertoCandidato`
--
ALTER TABLE `acertoCandidato`
  ADD CONSTRAINT `fk_acertoCandidato_desafios` FOREIGN KEY (`idDesafio`) REFERENCES `desafios` (`idDesafio`),
  ADD CONSTRAINT `fk_acertoCandidato_respostaSubmetida` FOREIGN KEY (`idRespostaSubmetida`) REFERENCES `respostaSubmetida` (`idRespSubmetida`);

--
-- Constraints for table `candidato`
--
ALTER TABLE `candidato`
  ADD CONSTRAINT `fk_candidato_contest1` FOREIGN KEY (`idContest`) REFERENCES `contest` (`idContest`);

--
-- Constraints for table `desafios`
--
ALTER TABLE `desafios`
  ADD CONSTRAINT `fk_desafios_contest` FOREIGN KEY (`idContest`) REFERENCES `contest` (`idContest`);

--
-- Constraints for table `equipe`
--
ALTER TABLE `equipe`
  ADD CONSTRAINT `fk_equipe_contest1` FOREIGN KEY (`idContest`) REFERENCES `contest` (`idContest`);

--
-- Constraints for table `equipe_has_candidato`
--
ALTER TABLE `equipe_has_candidato`
  ADD CONSTRAINT `fk_equipe_has_candidato_candidato1` FOREIGN KEY (`candidato_idCandidato`) REFERENCES `candidato` (`idCandidato`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_equipe_has_candidato_equipe1` FOREIGN KEY (`equipe_idEquipe`) REFERENCES `equipe` (`idEquipe`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `inativos`
--
ALTER TABLE `inativos`
  ADD CONSTRAINT `fk_inativos_desafios` FOREIGN KEY (`idDesafio`) REFERENCES `desafios` (`idDesafio`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `respDesafio`
--
ALTER TABLE `respDesafio`
  ADD CONSTRAINT `fk_respDesafio_desafios` FOREIGN KEY (`idDesafio`) REFERENCES `desafios` (`idDesafio`);

--
-- Constraints for table `respostaSubmetida`
--
ALTER TABLE `respostaSubmetida`
  ADD CONSTRAINT `fk_respostaSubmetida_candidato` FOREIGN KEY (`idCandidato`) REFERENCES `candidato` (`idCandidato`),
  ADD CONSTRAINT `fk_respostaSubmetida_desafios` FOREIGN KEY (`idDesafio`) REFERENCES `desafios` (`idDesafio`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

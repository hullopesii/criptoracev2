CREATE 
    ALGORITHM = UNDEFINED 
    DEFINER = `root`@`localhost` 
    SQL SECURITY DEFINER
VIEW `criptoRace`.`view_candidatos_do_contest` AS
    SELECT 
        `criptoRace`.`candidato`.`nomeCompleto` AS `nomeCompleto`,
        `criptoRace`.`candidato`.`nick` AS `nick`,
        (SELECT FUNCTION_GET_PONTUACAO(`criptoRace`.`candidato`.`idCandidato`)) AS `Pontuação`,
        (SELECT 
                COUNT(0)
            FROM
                `criptoRace`.`respostaSubmetida`
            WHERE
                (`criptoRace`.`respostaSubmetida`.`idCandidato` = `criptoRace`.`candidato`.`idCandidato`)) AS `qtdSubmissões`,
        (SELECT FUNCTION_GET_EQUIPE_DO_CANDIDATO(`criptoRace`.`candidato`.`idCandidato`)) AS `equipe`,
        (SELECT 
                COUNT(0)
            FROM
                `criptoRace`.`mensagens`
            WHERE
                (`criptoRace`.`mensagens`.`idCandidato` = `criptoRace`.`candidato`.`idCandidato`)) AS `qtdMensagens`,
        `criptoRace`.`candidato`.`idCandidato` AS `idCandidato`,
        `criptoRace`.`candidato`.`idContest` AS `idContest`
    FROM
        `criptoRace`.`candidato`
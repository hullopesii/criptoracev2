/* uso select function_cadastraCandidato("fulano", "eita", "996", "7", "fulano", "fulano", 1); onde 1 é o id do contest, que dever ter sido obtida anteriormente, baseando-se no contest ativo*/

DELIMITER $$
CREATE FUNCTION `function_cadastraCandidato`(
	nC VARCHAR(100),
    nick VARCHAR(100),
    matricula VARCHAR(30),
    cpf VARCHAR(15),
    usuario VARCHAR(45),
    pass VARCHAR(200),
    idC INT) RETURNS int(11)
BEGIN
	INSERT INTO `candidato`
		(nomeCompleto,nick,matriculaIFG,cpf,user,passWd,idContest)
        values
        (nC,nick,matricula,cpf,usuario,pass,idC);
       return LAST_INSERT_ID();
END$$
DELIMITER ;


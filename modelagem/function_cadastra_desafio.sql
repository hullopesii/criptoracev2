CREATE FUNCTION `function_cadastra_desafio` (
	descD varchar(300),
    idC int(11),
    pontuacao int(10),
    tpDesafio varchar(20),
    idEquipe int(11)
    )
RETURNS INTEGER
BEGIN
	insert into desafios values
    (null,
	descD,
	idC,
    "Inativo",
	pontuacao,
	tpDesafio,
	idEquipe);
RETURN LAST_INSERT_ID();
END

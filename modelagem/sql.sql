-- MySQL dump 10.13  Distrib 5.7.19, for Linux (x86_64)
--
-- Host: localhost    Database: criptoRace
-- ------------------------------------------------------
-- Server version	5.7.19-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `acertoCandidato`
--

DROP TABLE IF EXISTS `acertoCandidato`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `acertoCandidato` (
  `idAcertos` int(11) NOT NULL AUTO_INCREMENT,
  `idDesafio` int(11) DEFAULT NULL,
  `idRespostaSubmetida` int(11) DEFAULT NULL,
  PRIMARY KEY (`idAcertos`),
  KEY `fk_acertoCandidato_desafios` (`idDesafio`),
  KEY `fk_acertoCandidato_respostaSubmetida` (`idRespostaSubmetida`),
  CONSTRAINT `fk_acertoCandidato_desafios` FOREIGN KEY (`idDesafio`) REFERENCES `desafios` (`idDesafio`),
  CONSTRAINT `fk_acertoCandidato_respostaSubmetida` FOREIGN KEY (`idRespostaSubmetida`) REFERENCES `respostaSubmetida` (`idRespSubmetida`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `acertoCandidato`
--

LOCK TABLES `acertoCandidato` WRITE;
/*!40000 ALTER TABLE `acertoCandidato` DISABLE KEYS */;
INSERT INTO `acertoCandidato` VALUES (1,1,1),(2,1,2),(3,2,3);
/*!40000 ALTER TABLE `acertoCandidato` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `candidato`
--

DROP TABLE IF EXISTS `candidato`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `candidato` (
  `idCandidato` int(11) NOT NULL AUTO_INCREMENT,
  `nomeCompleto` varchar(100) NOT NULL,
  `nick` varchar(100) DEFAULT NULL,
  `matriculaIFG` varchar(30) DEFAULT NULL,
  `cpf` varchar(15) DEFAULT NULL,
  `user` varchar(45) NOT NULL,
  `passWd` varchar(200) NOT NULL,
  `idContest` int(11) NOT NULL,
  PRIMARY KEY (`idCandidato`),
  UNIQUE KEY `matriculaIFG` (`matriculaIFG`),
  UNIQUE KEY `matriculaIFG_2` (`matriculaIFG`),
  KEY `cpf_2` (`cpf`),
  KEY `fk_candidato_contest1_idx` (`idContest`),
  CONSTRAINT `fk_candidato_contest1` FOREIGN KEY (`idContest`) REFERENCES `contest` (`idContest`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `candidato`
--

LOCK TABLES `candidato` WRITE;
/*!40000 ALTER TABLE `candidato` DISABLE KEYS */;
INSERT INTO `candidato` VALUES (1,'usuário teste','tester','99999999',NULL,'tester','tester',1),(2,'Usuário teste 2','tester2','888',NULL,'tester2','tester2',1),(3,'Usuário teste 3','tester3','4444',NULL,'tester3','tester3',1),(4,'Frederico','Fred','1234','2482094424242','fredcl','1234',1),(7,'Frederico','Fred','3','34114141','caetano','1',1),(8,'brendo','fsfs','3r24','5y3y35363','b','1',1),(9,'Frederico','f','13413432','12413','fred','1',1),(10,'Matheus','Thytuz','20141030090094','05675067116','Thytuz','123456',1),(12,'a','a','a','a','a','a',1),(14,'b','b','b','b','b','b',1),(16,'g','g','g','g','g','g',1);
/*!40000 ALTER TABLE `candidato` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contest`
--

DROP TABLE IF EXISTS `contest`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contest` (
  `idContest` int(11) NOT NULL AUTO_INCREMENT,
  `nomeContest` varchar(200) NOT NULL,
  `status` enum('Ativo','Inativo') DEFAULT NULL,
  `loc` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`idContest`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contest`
--

LOCK TABLES `contest` WRITE;
/*!40000 ALTER TABLE `contest` DISABLE KEYS */;
INSERT INTO `contest` VALUES (1,'teste','Ativo','ifg-inhumas');
/*!40000 ALTER TABLE `contest` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `desafios`
--

DROP TABLE IF EXISTS `desafios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `desafios` (
  `idDesafio` int(11) NOT NULL AUTO_INCREMENT,
  `descricaoDesafio` varchar(300) NOT NULL,
  `idContest` int(11) NOT NULL,
  `status` enum('Ativo','Inativo') NOT NULL,
  `pontuacao` int(1) NOT NULL,
  `tipoDesafio` enum('Individual','Equipe') DEFAULT NULL,
  `idEquipe` int(11) DEFAULT NULL COMMENT 'Id da equipe criadora, para o caso de desafios criados por equipes. Caso seja criado pelo adm, fica em branco.\n',
  PRIMARY KEY (`idDesafio`),
  KEY `fk_desafios_contest` (`idContest`),
  CONSTRAINT `fk_desafios_contest` FOREIGN KEY (`idContest`) REFERENCES `contest` (`idContest`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `desafios`
--

LOCK TABLES `desafios` WRITE;
/*!40000 ALTER TABLE `desafios` DISABLE KEYS */;
INSERT INTO `desafios` VALUES (1,'teste',1,'Ativo',1,'Individual',NULL),(2,'teste 2',1,'Ativo',2,'Individual',NULL);
/*!40000 ALTER TABLE `desafios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `equipe`
--

DROP TABLE IF EXISTS `equipe`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `equipe` (
  `idEquipe` int(11) NOT NULL AUTO_INCREMENT,
  `nomeEquipe` varchar(45) NOT NULL,
  `idContest` int(11) NOT NULL,
  PRIMARY KEY (`idEquipe`),
  KEY `fk_equipe_contest1_idx` (`idContest`),
  CONSTRAINT `fk_equipe_contest1` FOREIGN KEY (`idContest`) REFERENCES `contest` (`idContest`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `equipe`
--

LOCK TABLES `equipe` WRITE;
/*!40000 ALTER TABLE `equipe` DISABLE KEYS */;
INSERT INTO `equipe` VALUES (1,'Testadores',1);
/*!40000 ALTER TABLE `equipe` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `equipe_has_candidato`
--

DROP TABLE IF EXISTS `equipe_has_candidato`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `equipe_has_candidato` (
  `equipe_idEquipe` int(11) NOT NULL,
  `candidato_idCandidato` int(11) NOT NULL,
  `tipo` enum('lider','membro') DEFAULT NULL,
  PRIMARY KEY (`equipe_idEquipe`,`candidato_idCandidato`),
  KEY `fk_equipe_has_candidato_candidato1_idx` (`candidato_idCandidato`),
  KEY `fk_equipe_has_candidato_equipe1_idx` (`equipe_idEquipe`),
  CONSTRAINT `fk_equipe_has_candidato_candidato1` FOREIGN KEY (`candidato_idCandidato`) REFERENCES `candidato` (`idCandidato`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_equipe_has_candidato_equipe1` FOREIGN KEY (`equipe_idEquipe`) REFERENCES `equipe` (`idEquipe`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='representa  participação de um candidato em uma equipe, em um contest';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `equipe_has_candidato`
--

LOCK TABLES `equipe_has_candidato` WRITE;
/*!40000 ALTER TABLE `equipe_has_candidato` DISABLE KEYS */;
INSERT INTO `equipe_has_candidato` VALUES (1,1,'lider'),(1,2,'membro');
/*!40000 ALTER TABLE `equipe_has_candidato` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `inativos`
--

DROP TABLE IF EXISTS `inativos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `inativos` (
  `idInativos` int(11) NOT NULL AUTO_INCREMENT,
  `idDesafio` int(11) NOT NULL,
  PRIMARY KEY (`idInativos`),
  KEY `fk_inativos_desafios` (`idDesafio`),
  CONSTRAINT `fk_inativos_desafios` FOREIGN KEY (`idDesafio`) REFERENCES `desafios` (`idDesafio`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `inativos`
--

LOCK TABLES `inativos` WRITE;
/*!40000 ALTER TABLE `inativos` DISABLE KEYS */;
/*!40000 ALTER TABLE `inativos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `respDesafio`
--

DROP TABLE IF EXISTS `respDesafio`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `respDesafio` (
  `idResp` int(11) NOT NULL AUTO_INCREMENT,
  `resposta` longtext NOT NULL,
  `idDesafio` int(11) DEFAULT NULL,
  PRIMARY KEY (`idResp`),
  KEY `fk_respDesafio_desafios` (`idDesafio`),
  CONSTRAINT `fk_respDesafio_desafios` FOREIGN KEY (`idDesafio`) REFERENCES `desafios` (`idDesafio`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `respDesafio`
--

LOCK TABLES `respDesafio` WRITE;
/*!40000 ALTER TABLE `respDesafio` DISABLE KEYS */;
INSERT INTO `respDesafio` VALUES (1,'teste',1),(2,'teste2',2);
/*!40000 ALTER TABLE `respDesafio` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `respostaSubmetida`
--

DROP TABLE IF EXISTS `respostaSubmetida`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `respostaSubmetida` (
  `idRespSubmetida` int(11) NOT NULL AUTO_INCREMENT,
  `respostaSubmetida` longtext,
  `idCandidato` int(11) DEFAULT NULL,
  `idDesafio` int(11) DEFAULT NULL,
  PRIMARY KEY (`idRespSubmetida`),
  KEY `fk_respostaSubmetida_candidato` (`idCandidato`),
  KEY `fk_respostaSubmetida_desafios` (`idDesafio`),
  CONSTRAINT `fk_respostaSubmetida_candidato` FOREIGN KEY (`idCandidato`) REFERENCES `candidato` (`idCandidato`),
  CONSTRAINT `fk_respostaSubmetida_desafios` FOREIGN KEY (`idDesafio`) REFERENCES `desafios` (`idDesafio`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `respostaSubmetida`
--

LOCK TABLES `respostaSubmetida` WRITE;
/*!40000 ALTER TABLE `respostaSubmetida` DISABLE KEYS */;
INSERT INTO `respostaSubmetida` VALUES (1,'teste',1,1),(2,'teste2',2,1),(3,'teste2',1,2);
/*!40000 ALTER TABLE `respostaSubmetida` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `view_candidato`
--

DROP TABLE IF EXISTS `view_candidato`;
/*!50001 DROP VIEW IF EXISTS `view_candidato`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `view_candidato` AS SELECT 
 1 AS `idCandidato`,
 1 AS `nomeCompleto`,
 1 AS `nick`,
 1 AS `matriculaIFG`,
 1 AS `cpf`,
 1 AS `user`,
 1 AS `passWd`,
 1 AS `idContest`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `view_contestAtivo`
--

DROP TABLE IF EXISTS `view_contestAtivo`;
/*!50001 DROP VIEW IF EXISTS `view_contestAtivo`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `view_contestAtivo` AS SELECT 
 1 AS `idContest`,
 1 AS `nomeContest`,
 1 AS `status`,
 1 AS `loc`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `view_login`
--

DROP TABLE IF EXISTS `view_login`;
/*!50001 DROP VIEW IF EXISTS `view_login`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `view_login` AS SELECT 
 1 AS `idCandidato`,
 1 AS `nomeCompleto`,
 1 AS `nick`,
 1 AS `matriculaIFG`,
 1 AS `cpf`,
 1 AS `user`,
 1 AS `passWd`,
 1 AS `idContest`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `view_ranking`
--

DROP TABLE IF EXISTS `view_ranking`;
/*!50001 DROP VIEW IF EXISTS `view_ranking`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `view_ranking` AS SELECT 
 1 AS `id`,
 1 AS `nick`,
 1 AS `nomeCompleto`,
 1 AS `totalPontos`*/;
SET character_set_client = @saved_cs_client;

--
-- Final view structure for view `view_candidato`
--

/*!50001 DROP VIEW IF EXISTS `view_candidato`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `view_candidato` AS select `candidato`.`idCandidato` AS `idCandidato`,`candidato`.`nomeCompleto` AS `nomeCompleto`,`candidato`.`nick` AS `nick`,`candidato`.`matriculaIFG` AS `matriculaIFG`,`candidato`.`cpf` AS `cpf`,`candidato`.`user` AS `user`,`candidato`.`passWd` AS `passWd`,`candidato`.`idContest` AS `idContest` from `candidato` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `view_contestAtivo`
--

/*!50001 DROP VIEW IF EXISTS `view_contestAtivo`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `view_contestAtivo` AS select `contest`.`idContest` AS `idContest`,`contest`.`nomeContest` AS `nomeContest`,`contest`.`status` AS `status`,`contest`.`loc` AS `loc` from `contest` where (`contest`.`status` = 'Ativo') order by `contest`.`idContest` desc limit 0,1 */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `view_login`
--

/*!50001 DROP VIEW IF EXISTS `view_login`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `view_login` AS select `candidato`.`idCandidato` AS `idCandidato`,`candidato`.`nomeCompleto` AS `nomeCompleto`,`candidato`.`nick` AS `nick`,`candidato`.`matriculaIFG` AS `matriculaIFG`,`candidato`.`cpf` AS `cpf`,`candidato`.`user` AS `user`,`candidato`.`passWd` AS `passWd`,`candidato`.`idContest` AS `idContest` from `candidato` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `view_ranking`
--

/*!50001 DROP VIEW IF EXISTS `view_ranking`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `view_ranking` AS select `candidato`.`idCandidato` AS `id`,`candidato`.`nick` AS `nick`,`candidato`.`nomeCompleto` AS `nomeCompleto`,sum(`desafios`.`pontuacao`) AS `totalPontos` from (((`acertoCandidato` join `respostaSubmetida` on((`respostaSubmetida`.`idRespSubmetida` = `acertoCandidato`.`idRespostaSubmetida`))) join `desafios` on((`desafios`.`idDesafio` = `acertoCandidato`.`idDesafio`))) join `candidato` on((`candidato`.`idCandidato` = `respostaSubmetida`.`idCandidato`))) group by `candidato`.`idCandidato` order by `totalPontos` desc */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-09-14 11:17:17

USE `criptoRace`;
DROP function IF EXISTS `function_get_equipe_do_candidato`;

DELIMITER $$
USE `criptoRace`$$
CREATE FUNCTION `function_get_equipe_do_candidato` (idCandidato int(11))
RETURNS varchar(100)
BEGIN
	DECLARE nomeEquipe varchar(100);
    set nomeEquipe = (select concat(equipe.nomeEquipe, '-',equipe_has_candidato.tipo) as 'equipe' from equipe_has_candidato 
			inner join candidato on candidato.idCandidato = 
            equipe_has_candidato.candidato_idCandidato INNER join equipe on 
            equipe.idEquipe = equipe_has_candidato.equipe_idEquipe where 
            equipe_has_candidato.candidato_idCandidato=idCandidato);
RETURN nomeEquipe;
END$$

DELIMITER ;



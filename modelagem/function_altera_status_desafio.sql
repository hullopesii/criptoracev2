CREATE FUNCTION `function_altera_status_desafio`(
	novoStatus varchar(20),
    idD int(11)) RETURNS int(11)
BEGIN
	if(novoStatus = "Ativo" or novoStatus = "Inativo") then
		update desafios set status = novoStatus where idDesafio = idD;
        RETURN ROW_COUNT();
    else
		return 0;
    end if;
END
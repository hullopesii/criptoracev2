USE `criptoRace`;
DROP function IF EXISTS `function_salva_resposta_submetida`;

DELIMITER $$
USE `criptoRace`$$
CREATE FUNCTION `function_salva_resposta_submetida`(resp longtext,idC int(11)) RETURNS int(11)
BEGIN
	insert into respostaSubmetida values (null, resp, idC, null);
RETURN (SELECT LAST_INSERT_ID());

END$$

DELIMITER ;

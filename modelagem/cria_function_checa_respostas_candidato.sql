USE `criptoRace`;
DROP function IF EXISTS `function_checa_respostas_candidato`;

DELIMITER $$
USE `criptoRace`$$
CREATE FUNCTION `function_checa_respostas_candidato`(idC int(11),resp longtext) RETURNS int(11)
BEGIN
DECLARE cont int(11);
	set cont = (SELECT count(*) as 'total' FROM `respostaSubmetida` inner join 
		acertoCandidato on acertoCandidato.idRespostaSubmetida = 
		respostaSubmetida.idRespSubmetida 
		WHERE (respostaSubmetida.respostaSubmetida= resp and 
		respostaSubmetida.idCandidato=idC));
	return cont;

END$$

DELIMITER ;



CREATE 
VIEW `criptoRace`.`view_login_adm` AS
    SELECT 
        `criptoRace`.`administrador`.`idCandidato` AS `idCandidato`,
        `criptoRace`.`administrador`.`nomeCompleto` AS `nomeCompleto`,
        `criptoRace`.`administrador`.`nick` AS `nick`,
        `criptoRace`.`administrador`.`matriculaIFG` AS `matriculaIFG`,
        `criptoRace`.`administrador`.`cpf` AS `cpf`,
        `criptoRace`.`administrador`.`user` AS `user`,
        `criptoRace`.`administrador`.`passWd` AS `passWd`
    FROM
        `criptoRace`.`administrador`
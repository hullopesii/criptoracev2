use criptoRace;

CREATE TABLE IF NOT EXISTS `criptoRace`.`administrador` (
  `idCandidato` INT(11) NOT NULL AUTO_INCREMENT,
  `nomeCompleto` VARCHAR(100) NOT NULL,
  `nick` VARCHAR(100) NULL DEFAULT NULL,
  `matriculaIFG` VARCHAR(30) NULL DEFAULT NULL,
  `cpf` VARCHAR(15) NULL DEFAULT NULL,
  `user` VARCHAR(400) NOT NULL,
  `passWd` VARCHAR(400) NOT NULL,
  PRIMARY KEY (`idCandidato`),
  UNIQUE INDEX `matriculaIFG` (`matriculaIFG` ASC),
  UNIQUE INDEX `matriculaIFG_2` (`matriculaIFG` ASC),
  INDEX `cpf_2` (`cpf` ASC),
  UNIQUE INDEX `user_UNIQUE` (`user` ASC))
ENGINE = InnoDB
AUTO_INCREMENT = 1
DEFAULT CHARACTER SET = latin1;

Insert into administrador values (null, "Tester", "testador", "000", "","admin","admin");


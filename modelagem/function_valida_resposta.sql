CREATE DEFINER=`root`@`localhost` FUNCTION `function_valida_resposta`(resp longtext, idC int(11)) RETURNS int(11)
BEGIN	
 DECLARE pontos int(11);
 DECLARE idD int(11);
 
 set pontos = (pegaPontuacao(resp, idC));
  
 if(pontos <> null or pontos > 0)then
	set idD = (pegaIdDesafio(resp, idC));
	if(isInactively(idD))then		
        /*update desafios set status = "Inativo" where idDesafio = idD;*/
        call procedure_inativaDesafio(idD);
    end if;
	RETURN pontos;
 else
	RETURN 0;
 end if;

END
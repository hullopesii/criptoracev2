/* uso: select * from view_login where (user='' and passWd='' and idContest=1)  em que o idContest deve ser buscado anteriormente entre os contests ativos*/

--
-- Database: `criptoRace`
--

-- --------------------------------------------------------

use criptoRace;

--
-- Stand-in structure for view `view_login`
--
CREATE TABLE `view_login` (
`idCandidato` int(11)
,`nomeCompleto` varchar(100)
,`nick` varchar(100)
,`matriculaIFG` varchar(30)
,`cpf` varchar(15)
,`user` varchar(45)
,`passWd` varchar(200)
,`idContest` int(11)
);

-- --------------------------------------------------------

--
-- Structure for view `view_login`
--
DROP TABLE IF EXISTS `view_login`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_login`  AS  select `candidato`.`idCandidato` AS `idCandidato`,`candidato`.`nomeCompleto` AS `nomeCompleto`,`candidato`.`nick` AS `nick`,`candidato`.`matriculaIFG` AS `matriculaIFG`,`candidato`.`cpf` AS `cpf`,`candidato`.`user` AS `user`,`candidato`.`passWd` AS `passWd`,`candidato`.`idContest` AS `idContest` from `candidato` limit 0,1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

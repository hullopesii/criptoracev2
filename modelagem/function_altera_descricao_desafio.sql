CREATE FUNCTION `function_altera_descricao_desafio`(
	novaDescricao varchar(300),
    idD int(11)) RETURNS int(11)
BEGIN
	update desafios set descricaoDesafio = novaDescricao where idDesafio = idD;
RETURN ROW_COUNT();
END
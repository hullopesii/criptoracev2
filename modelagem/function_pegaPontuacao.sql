CREATE FUNCTION `pegaPontuacao`(resp longtext, idC int(11)) RETURNS int(11)
BEGIN
	DECLARE pontuacao int(11);
    set pontuacao =(select desafios.pontuacao from respDesafio inner join desafios on desafios.idDesafio = respDesafio.idDesafio
		where (desafios.status= "Ativo" and respDesafio.resposta = resp and desafios.idContest = idC) Limit 1);
RETURN pontuacao;
END
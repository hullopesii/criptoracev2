CREATE TABLE IF NOT EXISTS `criptoRace`.`mensagens` (
  `idMensagens` INT NOT NULL AUTO_INCREMENT,
  `mensagem` VARCHAR(140) NOT NULL,
  `idCandidato` INT(11) NOT NULL,
  PRIMARY KEY (`idMensagens`),
  INDEX `fk_mensagens_candidato1_idx` (`idCandidato` ASC),
  CONSTRAINT `fk_mensagens_candidato1`
    FOREIGN KEY (`idCandidato`)
    REFERENCES `criptoRace`.`candidato` (`idCandidato`)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT)
ENGINE = InnoDB

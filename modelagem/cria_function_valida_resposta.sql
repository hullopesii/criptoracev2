USE `criptoRace`;
DROP function IF EXISTS `function_valida_resposta`;

DELIMITER $$
USE `criptoRace`$$
CREATE FUNCTION `function_valida_resposta`(resp longtext, idC int(11)) RETURNS int(11)
BEGIN	
 DECLARE pontos int(11);
 DECLARE idD int(11);
 DECLARE idRS int(11);
 DECLARE t varchar(100);
 DECLARE idContest int(11);
 
 set idContest = (SELECT contest.idContest FROM candidato inner join contest on contest.idContest =candidato.idContest where candidato.idCandidato = idC);
 set pontos = (pegaPontuacao(resp, idContest));
 set idRS = (function_salva_resposta_submetida(resp, idC)); 
  
 if(pontos <> null or pontos > 0)then
	set idD = (pegaIdDesafio(resp, idContest));
    /*return function_checa_respostas_candidato(idC,resp);*/
    if (function_checa_respostas_candidato(idC,resp)>0)then
        set pontos = -1;    
	else		
        call procedure_atribui_pontuacao(idD,idRS);  
	end if;
	if(isInactively(idD))then		
        call procedure_inativaDesafio(idD);        
    end if;
	RETURN pontos;
 else
	RETURN 0;
 end if;

END$$

DELIMITER ;



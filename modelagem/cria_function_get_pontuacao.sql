USE `criptoRace`;
DROP function IF EXISTS `function_get_pontuacao`;

DELIMITER $$
USE `criptoRace`$$
CREATE FUNCTION `function_get_pontuacao`(idCandidato int(11)) RETURNS int(11)
BEGIN
	return (SELECT 
        SUM(`criptoRace`.`desafios`.`pontuacao`) AS `Pontuação`
    FROM
        (((`criptoRace`.`acertoCandidato`
        JOIN `criptoRace`.`respostaSubmetida` ON ((`criptoRace`.`respostaSubmetida`.`idRespSubmetida` = `criptoRace`.`acertoCandidato`.`idRespostaSubmetida`)))
        JOIN `criptoRace`.`desafios` ON ((`criptoRace`.`desafios`.`idDesafio` = `criptoRace`.`acertoCandidato`.`idDesafio`)))
        JOIN `criptoRace`.`candidato` ON ((`criptoRace`.`candidato`.`idCandidato` = `criptoRace`.`respostaSubmetida`.`idCandidato`)))
    where candidato.idCandidato=idCandidato
    GROUP BY `criptoRace`.`candidato`.`idCandidato`);
END$$

DELIMITER ;



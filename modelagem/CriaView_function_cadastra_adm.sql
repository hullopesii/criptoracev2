CREATE DEFINER=`root`@`localhost` FUNCTION `function_cadastraAdm`(
	nC VARCHAR(100),
    nick VARCHAR(100),
    matricula VARCHAR(30),
    cpf VARCHAR(15),
    usuario VARCHAR(400),
    pass VARCHAR(400),
    uL 	VARCHAR(400),
    pL VARCHAR(400)) RETURNS int(11)
    DETERMINISTIC
BEGIN
	if(hasAdm(uL,pL)>0)then
        if((SELECT count(*) FROM administrador WHERE (user = usuario))=0)then
			INSERT INTO `administrador`
				(nomeCompleto,nick,matriculaIFG,cpf,user,passWd)
				values
				(nC,nick,matricula,cpf,usuario,pass);
				return LAST_INSERT_ID();
        else
			return -1;
        end if;    
     else
			return 0;
     end if;   
END
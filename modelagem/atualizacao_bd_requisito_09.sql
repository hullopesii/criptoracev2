USE `criptoRace`;
DROP function IF EXISTS `function_pega_erros`;

DELIMITER $$
USE `criptoRace`$$
CREATE FUNCTION `function_pega_erros` (idCandidato int(11))
RETURNS INTEGER
BEGIN
Declare total int(11) default 0;

set total= (
SELECT 
        COUNT(*) AS `total`
    FROM
        (`criptoRace`.`respostaSubmetida`
        JOIN `criptoRace`.`candidato` ON ((`criptoRace`.`candidato`.`idCandidato` = `criptoRace`.`respostaSubmetida`.`idCandidato`)))
    WHERE
        (NOT (`criptoRace`.`respostaSubmetida`.`idRespSubmetida` IN (SELECT 
                `criptoRace`.`acertoCandidato`.`idRespostaSubmetida`
            FROM
                `criptoRace`.`acertoCandidato`)) and candidato.idCandidato=idCandidato)
                
    GROUP BY `criptoRace`.`respostaSubmetida`.`idCandidato`
    );
    if(total is null)then
		return 0;
    end if;
RETURN total;
END$$

DELIMITER ;

USE `criptoRace`;
DROP function IF EXISTS `function_pega_acertos`;

DELIMITER $$
USE `criptoRace`$$
CREATE FUNCTION `function_pega_acertos` (idCandidato int(11))
RETURNS INTEGER
BEGIN
Declare total int(11) default 0;

set total = (select count(*) from acertoCandidato inner join respostaSubmetida on 
	respostaSubmetida.idRespSubmetida = acertoCandidato.idRespostaSubmetida where 
    respostaSubmetida.idCandidato = idCandidato);

if(total is null)then
return 0;
end if;

RETURN total;
END$$

DELIMITER ;


DROP view IF EXISTS `view_ranking_individual`;

CREATE 
    ALGORITHM = UNDEFINED 
    DEFINER = `root`@`localhost` 
    SQL SECURITY DEFINER
VIEW `criptoRace`.`view_ranking_individual` AS
    SELECT 
        `criptoRace`.`candidato`.`idCandidato` AS `id`,
        `criptoRace`.`candidato`.`nick` AS `nick`,
        `criptoRace`.`candidato`.`nomeCompleto` AS `nomeCompleto`,
        SUM(`criptoRace`.`desafios`.`pontuacao`) AS `totalPontos`,
        (SELECT FUNCTION_PEGA_ERROS(`criptoRace`.`candidato`.`idCandidato`)) AS `erros`,
        (SELECT FUNCTION_PEGA_ACERTOS(`criptoRace`.`candidato`.`idCandidato`)) AS `acertos`
    FROM
        (((`criptoRace`.`acertoCandidato`
        JOIN `criptoRace`.`respostaSubmetida` ON ((`criptoRace`.`respostaSubmetida`.`idRespSubmetida` = `criptoRace`.`acertoCandidato`.`idRespostaSubmetida`)))
        JOIN `criptoRace`.`desafios` ON ((`criptoRace`.`desafios`.`idDesafio` = `criptoRace`.`acertoCandidato`.`idDesafio`)))
        JOIN `criptoRace`.`candidato` ON ((`criptoRace`.`candidato`.`idCandidato` = `criptoRace`.`respostaSubmetida`.`idCandidato`)))
    GROUP BY `criptoRace`.`candidato`.`idCandidato`



